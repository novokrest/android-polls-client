/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.api;

import polls.client.ApiInvoker;
import polls.client.ApiException;
import polls.client.Pair;

import polls.client.model.*;

import java.util.*;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import polls.client.model.Contact;
import polls.client.model.ContactList;
import polls.client.model.ErrorResponse;
import polls.client.model.ListPoll;
import polls.client.model.PollRequest;
import polls.client.model.PollResult;
import polls.client.model.SearchOptions;
import polls.client.model.UserIdModel;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class PublicPollApi {
  String basePath = "http://demo.honeyshyam.com/api";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public void addHeader(String key, String value) {
    getInvoker().addDefaultHeader(key, value);
  }

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }

  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }

  public String getBasePath() {
    return basePath;
  }

  /**
  * Get the data from our service. It will requires hearder kyes userId and token and key pollId
  * Get the data from our service. It will requires hearder kyes userId and token and key pollId
   * @param pollRequest 
   * @return PollResult
  */
  public PollResult getPollResult (PollRequest pollRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = pollRequest;
    // verify the required parameter 'pollRequest' is set
    if (pollRequest == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'pollRequest' when calling getPollResult",
        new ApiException(400, "Missing the required parameter 'pollRequest' when calling getPollResult"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/GetPollResult";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (PollResult) ApiInvoker.deserialize(localVarResponse, "", PollResult.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Get the data from our service. It will requires hearder kyes userId and token and key pollId
   * Get the data from our service. It will requires hearder kyes userId and token and key pollId
   * @param pollRequest 
  */
  public void getPollResult (PollRequest pollRequest, final Response.Listener<PollResult> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = pollRequest;

    // verify the required parameter 'pollRequest' is set
    if (pollRequest == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'pollRequest' when calling getPollResult",
        new ApiException(400, "Missing the required parameter 'pollRequest' when calling getPollResult"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/GetPollResult".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((PollResult) ApiInvoker.deserialize(localVarResponse,  "", PollResult.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
  * This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
   * @param searchOptions 
   * @return ListPoll
  */
  public ListPoll getPublic (SearchOptions searchOptions) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = searchOptions;
    // verify the required parameter 'searchOptions' is set
    if (searchOptions == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'searchOptions' when calling getPublic",
        new ApiException(400, "Missing the required parameter 'searchOptions' when calling getPublic"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/GetPublic";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (ListPoll) ApiInvoker.deserialize(localVarResponse, "", ListPoll.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
   * This is a public API to Get Public Polls for the request parameter             View all Public polls, View by Category, View by Username etc.
   * @param searchOptions 
  */
  public void getPublic (SearchOptions searchOptions, final Response.Listener<ListPoll> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = searchOptions;

    // verify the required parameter 'searchOptions' is set
    if (searchOptions == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'searchOptions' when calling getPublic",
        new ApiException(400, "Missing the required parameter 'searchOptions' when calling getPublic"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/GetPublic".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((ListPoll) ApiInvoker.deserialize(localVarResponse,  "", ListPoll.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Lists the public users.
  * Lists the public users.
   * @param searchOptions 
   * @return ContactList
  */
  public ContactList listPublicUsers (SearchOptions searchOptions) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = searchOptions;
    // verify the required parameter 'searchOptions' is set
    if (searchOptions == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'searchOptions' when calling listPublicUsers",
        new ApiException(400, "Missing the required parameter 'searchOptions' when calling listPublicUsers"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/ListPublicUsers";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (ContactList) ApiInvoker.deserialize(localVarResponse, "", ContactList.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Lists the public users.
   * Lists the public users.
   * @param searchOptions 
  */
  public void listPublicUsers (SearchOptions searchOptions, final Response.Listener<ContactList> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = searchOptions;

    // verify the required parameter 'searchOptions' is set
    if (searchOptions == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'searchOptions' when calling listPublicUsers",
        new ApiException(400, "Missing the required parameter 'searchOptions' when calling listPublicUsers"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/ListPublicUsers".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((ContactList) ApiInvoker.deserialize(localVarResponse,  "", ContactList.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * View Public Profile details for the unique identifier, rest follow the ViewProfile.
  * View Public Profile details for the unique identifier, rest follow the ViewProfile.
   * @param userIdModel 
   * @return Contact
  */
  public Contact viewPublicProfile (UserIdModel userIdModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = userIdModel;
    // verify the required parameter 'userIdModel' is set
    if (userIdModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'userIdModel' when calling viewPublicProfile",
        new ApiException(400, "Missing the required parameter 'userIdModel' when calling viewPublicProfile"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/ViewPublicProfile";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (Contact) ApiInvoker.deserialize(localVarResponse, "", Contact.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * View Public Profile details for the unique identifier, rest follow the ViewProfile.
   * View Public Profile details for the unique identifier, rest follow the ViewProfile.
   * @param userIdModel 
  */
  public void viewPublicProfile (UserIdModel userIdModel, final Response.Listener<Contact> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = userIdModel;

    // verify the required parameter 'userIdModel' is set
    if (userIdModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'userIdModel' when calling viewPublicProfile",
        new ApiException(400, "Missing the required parameter 'userIdModel' when calling viewPublicProfile"));
    }

    // create path and map variables
    String path = "/v1/PublicPoll/ViewPublicProfile".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((Contact) ApiInvoker.deserialize(localVarResponse,  "", Contact.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
}
