/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.api;

import polls.client.ApiInvoker;
import polls.client.ApiException;
import polls.client.Pair;

import polls.client.model.*;

import java.util.*;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import polls.client.model.CustomNotification;
import polls.client.model.ErrorResponse;
import polls.client.model.GCMViewModel;
import polls.client.model.ListCustomNotifications;
import polls.client.model.NotificationTrackingModel;
import polls.client.model.RegisterDeviceSNS;
import polls.client.model.RequestId;
import polls.client.model.SendMessageViewModel;
import polls.client.model.StatusResponse;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class PushApi {
  String basePath = "http://demo.honeyshyam.com/api";
  ApiInvoker apiInvoker = ApiInvoker.getInstance();

  public void addHeader(String key, String value) {
    getInvoker().addDefaultHeader(key, value);
  }

  public ApiInvoker getInvoker() {
    return apiInvoker;
  }

  public void setBasePath(String basePath) {
    this.basePath = basePath;
  }

  public String getBasePath() {
    return basePath;
  }

  /**
  * Gets the active custom notifications.
  * Gets the active custom notifications.
   * @param requestId 
   * @return ListCustomNotifications
  */
  public ListCustomNotifications activeNotifications (RequestId requestId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = requestId;
    // verify the required parameter 'requestId' is set
    if (requestId == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'requestId' when calling activeNotifications",
        new ApiException(400, "Missing the required parameter 'requestId' when calling activeNotifications"));
    }

    // create path and map variables
    String path = "/v1/Push/ActiveNotifications";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (ListCustomNotifications) ApiInvoker.deserialize(localVarResponse, "", ListCustomNotifications.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Gets the active custom notifications.
   * Gets the active custom notifications.
   * @param requestId 
  */
  public void activeNotifications (RequestId requestId, final Response.Listener<ListCustomNotifications> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = requestId;

    // verify the required parameter 'requestId' is set
    if (requestId == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'requestId' when calling activeNotifications",
        new ApiException(400, "Missing the required parameter 'requestId' when calling activeNotifications"));
    }

    // create path and map variables
    String path = "/v1/Push/ActiveNotifications".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((ListCustomNotifications) ApiInvoker.deserialize(localVarResponse,  "", ListCustomNotifications.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Lists the notification alerts.
  * Lists the notification alerts.
   * @param requestId 
   * @return List<NotificationTrackingModel>
  */
  public List<NotificationTrackingModel> listNotificationAlerts (RequestId requestId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = requestId;
    // verify the required parameter 'requestId' is set
    if (requestId == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'requestId' when calling listNotificationAlerts",
        new ApiException(400, "Missing the required parameter 'requestId' when calling listNotificationAlerts"));
    }

    // create path and map variables
    String path = "/v1/Push/ListNotificationAlerts";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (List<NotificationTrackingModel>) ApiInvoker.deserialize(localVarResponse, "array", NotificationTrackingModel.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Lists the notification alerts.
   * Lists the notification alerts.
   * @param requestId 
  */
  public void listNotificationAlerts (RequestId requestId, final Response.Listener<List<NotificationTrackingModel>> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = requestId;

    // verify the required parameter 'requestId' is set
    if (requestId == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'requestId' when calling listNotificationAlerts",
        new ApiException(400, "Missing the required parameter 'requestId' when calling listNotificationAlerts"));
    }

    // create path and map variables
    String path = "/v1/Push/ListNotificationAlerts".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((List<NotificationTrackingModel>) ApiInvoker.deserialize(localVarResponse,  "array", NotificationTrackingModel.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Registers the SNS/Push Notification device.
  * Registers the SNS/Push Notification device.
   * @param registerDeviceSNS 
   * @return String
  */
  public String registerSnsDevice (RegisterDeviceSNS registerDeviceSNS) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = registerDeviceSNS;
    // verify the required parameter 'registerDeviceSNS' is set
    if (registerDeviceSNS == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'registerDeviceSNS' when calling registerSnsDevice",
        new ApiException(400, "Missing the required parameter 'registerDeviceSNS' when calling registerSnsDevice"));
    }

    // create path and map variables
    String path = "/v1/Push/RegisterSnsDevice";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (String) ApiInvoker.deserialize(localVarResponse, "", String.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Registers the SNS/Push Notification device.
   * Registers the SNS/Push Notification device.
   * @param registerDeviceSNS 
  */
  public void registerSnsDevice (RegisterDeviceSNS registerDeviceSNS, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = registerDeviceSNS;

    // verify the required parameter 'registerDeviceSNS' is set
    if (registerDeviceSNS == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'registerDeviceSNS' when calling registerSnsDevice",
        new ApiException(400, "Missing the required parameter 'registerDeviceSNS' when calling registerSnsDevice"));
    }

    // create path and map variables
    String path = "/v1/Push/RegisterSnsDevice".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((String) ApiInvoker.deserialize(localVarResponse,  "", String.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Sends the web push notifications FCM test.
  * Sends the web push notifications FCM test.
   * @param gCMViewModel 
   * @return String
  */
  public String sendWebPushNotificationsFCMTest (GCMViewModel gCMViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = gCMViewModel;
    // verify the required parameter 'gCMViewModel' is set
    if (gCMViewModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'gCMViewModel' when calling sendWebPushNotificationsFCMTest",
        new ApiException(400, "Missing the required parameter 'gCMViewModel' when calling sendWebPushNotificationsFCMTest"));
    }

    // create path and map variables
    String path = "/v1/Push/SendWebPushNotificationsFCMTest";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (String) ApiInvoker.deserialize(localVarResponse, "", String.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Sends the web push notifications FCM test.
   * Sends the web push notifications FCM test.
   * @param gCMViewModel 
  */
  public void sendWebPushNotificationsFCMTest (GCMViewModel gCMViewModel, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = gCMViewModel;

    // verify the required parameter 'gCMViewModel' is set
    if (gCMViewModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'gCMViewModel' when calling sendWebPushNotificationsFCMTest",
        new ApiException(400, "Missing the required parameter 'gCMViewModel' when calling sendWebPushNotificationsFCMTest"));
    }

    // create path and map variables
    String path = "/v1/Push/SendWebPushNotificationsFCMTest".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((String) ApiInvoker.deserialize(localVarResponse,  "", String.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Sends the web push notifications test.
  * Sends the web push notifications test.
   * @param sendMessageViewModel 
   * @return String
  */
  public String sendWebPushNotificationsTest (SendMessageViewModel sendMessageViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = sendMessageViewModel;
    // verify the required parameter 'sendMessageViewModel' is set
    if (sendMessageViewModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'sendMessageViewModel' when calling sendWebPushNotificationsTest",
        new ApiException(400, "Missing the required parameter 'sendMessageViewModel' when calling sendWebPushNotificationsTest"));
    }

    // create path and map variables
    String path = "/v1/Push/SendWebPushNotificationsTest";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (String) ApiInvoker.deserialize(localVarResponse, "", String.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Sends the web push notifications test.
   * Sends the web push notifications test.
   * @param sendMessageViewModel 
  */
  public void sendWebPushNotificationsTest (SendMessageViewModel sendMessageViewModel, final Response.Listener<String> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = sendMessageViewModel;

    // verify the required parameter 'sendMessageViewModel' is set
    if (sendMessageViewModel == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'sendMessageViewModel' when calling sendWebPushNotificationsTest",
        new ApiException(400, "Missing the required parameter 'sendMessageViewModel' when calling sendWebPushNotificationsTest"));
    }

    // create path and map variables
    String path = "/v1/Push/SendWebPushNotificationsTest".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((String) ApiInvoker.deserialize(localVarResponse,  "", String.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
  /**
  * Updates the notification.
  * Updates the notification.
   * @param customNotification 
   * @return StatusResponse
  */
  public StatusResponse updateNotification (CustomNotification customNotification) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
    Object postBody = customNotification;
    // verify the required parameter 'customNotification' is set
    if (customNotification == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'customNotification' when calling updateNotification",
        new ApiException(400, "Missing the required parameter 'customNotification' when calling updateNotification"));
    }

    // create path and map variables
    String path = "/v1/Push/UpdateNotification";

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();
    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
    }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      String localVarResponse = apiInvoker.invokeAPI (basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames);
      if (localVarResponse != null) {
         return (StatusResponse) ApiInvoker.deserialize(localVarResponse, "", StatusResponse.class);
      } else {
         return null;
      }
    } catch (ApiException ex) {
       throw ex;
    } catch (InterruptedException ex) {
       throw ex;
    } catch (ExecutionException ex) {
      if (ex.getCause() instanceof VolleyError) {
        VolleyError volleyError = (VolleyError)ex.getCause();
        if (volleyError.networkResponse != null) {
          throw new ApiException(volleyError.networkResponse.statusCode, volleyError.getMessage());
        }
      }
      throw ex;
    } catch (TimeoutException ex) {
      throw ex;
    }
  }

      /**
   * Updates the notification.
   * Updates the notification.
   * @param customNotification 
  */
  public void updateNotification (CustomNotification customNotification, final Response.Listener<StatusResponse> responseListener, final Response.ErrorListener errorListener) {
    Object postBody = customNotification;

    // verify the required parameter 'customNotification' is set
    if (customNotification == null) {
      VolleyError error = new VolleyError("Missing the required parameter 'customNotification' when calling updateNotification",
        new ApiException(400, "Missing the required parameter 'customNotification' when calling updateNotification"));
    }

    // create path and map variables
    String path = "/v1/Push/UpdateNotification".replaceAll("\\{format\\}","json");

    // query params
    List<Pair> queryParams = new ArrayList<Pair>();
    // header params
    Map<String, String> headerParams = new HashMap<String, String>();
    // form params
    Map<String, String> formParams = new HashMap<String, String>();



    String[] contentTypes = {
      "application/json"
    };
    String contentType = contentTypes.length > 0 ? contentTypes[0] : "application/json";

    if (contentType.startsWith("multipart/form-data")) {
      // file uploading
      MultipartEntityBuilder localVarBuilder = MultipartEntityBuilder.create();
      

      HttpEntity httpEntity = localVarBuilder.build();
      postBody = httpEntity;
    } else {
      // normal form params
          }

    String[] authNames = new String[] { "apiToken", "apiUserId" };

    try {
      apiInvoker.invokeAPI(basePath, path, "POST", queryParams, postBody, headerParams, formParams, contentType, authNames,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String localVarResponse) {
            try {
              responseListener.onResponse((StatusResponse) ApiInvoker.deserialize(localVarResponse,  "", StatusResponse.class));
            } catch (ApiException exception) {
               errorListener.onErrorResponse(new VolleyError(exception));
            }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            errorListener.onErrorResponse(error);
          }
      });
    } catch (ApiException ex) {
      errorListener.onErrorResponse(new VolleyError(ex));
    }
  }
}
