/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * 
 **/
@ApiModel(description = "")
public class StatusResponse {
  
  @SerializedName("status")
  private String status = null;
  @SerializedName("statusMessage")
  private String statusMessage = null;

  /**
   * 
   **/
  @ApiModelProperty(value = "")
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * 
   **/
  @ApiModelProperty(value = "")
  public String getStatusMessage() {
    return statusMessage;
  }
  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatusResponse statusResponse = (StatusResponse) o;
    return (this.status == null ? statusResponse.status == null : this.status.equals(statusResponse.status)) &&
        (this.statusMessage == null ? statusResponse.statusMessage == null : this.statusMessage.equals(statusResponse.statusMessage));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.status == null ? 0: this.status.hashCode());
    result = 31 * result + (this.statusMessage == null ? 0: this.statusMessage.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class StatusResponse {\n");
    
    sb.append("  status: ").append(status).append("\n");
    sb.append("  statusMessage: ").append(statusMessage).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
