/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import java.util.Date;
import java.util.UUID;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * List all polls created by you.
 **/
@ApiModel(description = "List all polls created by you.")
public class ListPollModel {
  
  @SerializedName("pollId")
  private Long pollId = null;
  @SerializedName("hitId")
  private String hitId = null;
  @SerializedName("pollTypeId")
  private Long pollTypeId = null;
  @SerializedName("createDate")
  private Date createDate = null;
  @SerializedName("pollStatus")
  private String pollStatus = null;
  @SerializedName("pollTitle")
  private String pollTitle = null;
  @SerializedName("question")
  private String question = null;
  @SerializedName("keywords")
  private String keywords = null;
  @SerializedName("expirationDate")
  private Date expirationDate = null;
  @SerializedName("pollOverview")
  private String pollOverview = null;
  @SerializedName("firstOption")
  private String firstOption = null;
  @SerializedName("secondOption")
  private String secondOption = null;
  @SerializedName("firstImagePath")
  private String firstImagePath = null;
  @SerializedName("secondImagePath")
  private String secondImagePath = null;
  @SerializedName("assignmentDurationInSeconds")
  private Long assignmentDurationInSeconds = null;
  @SerializedName("maxAssignments")
  private Long maxAssignments = null;
  @SerializedName("isPublished")
  private Boolean isPublished = null;
  @SerializedName("lifeTimeInSeconds")
  private Long lifeTimeInSeconds = null;
  @SerializedName("choiceFirstCounter")
  private Long choiceFirstCounter = null;
  @SerializedName("choiceSecondCounter")
  private Long choiceSecondCounter = null;
  @SerializedName("resultCounter")
  private Long resultCounter = null;
  @SerializedName("isFilterOption")
  private Boolean isFilterOption = null;
  @SerializedName("filterMainCategory")
  private String filterMainCategory = null;
  @SerializedName("filtersJson")
  private String filtersJson = null;
  @SerializedName("isEnablePublic")
  private Boolean isEnablePublic = null;
  @SerializedName("catId")
  private Long catId = null;
  @SerializedName("catName")
  private String catName = null;
  @SerializedName("userName")
  private String userName = null;
  @SerializedName("ownerId")
  private UUID ownerId = null;
  @SerializedName("totalCount")
  private Long totalCount = null;
  @SerializedName("isAdult")
  private Boolean isAdult = null;
  @SerializedName("completedOn")
  private Date completedOn = null;
  @SerializedName("tags")
  private String tags = null;
  @SerializedName("alllowedAnonymously")
  private Integer alllowedAnonymously = null;
  @SerializedName("isEnabledComments")
  private Boolean isEnabledComments = null;
  @SerializedName("demographicCompleted")
  private Long demographicCompleted = null;

  /**
   * the poll identifier.
   **/
  @ApiModelProperty(value = "the poll identifier.")
  public Long getPollId() {
    return pollId;
  }
  public void setPollId(Long pollId) {
    this.pollId = pollId;
  }

  /**
   * the hit identifier.
   **/
  @ApiModelProperty(value = "the hit identifier.")
  public String getHitId() {
    return hitId;
  }
  public void setHitId(String hitId) {
    this.hitId = hitId;
  }

  /**
   * The poll type Id value can be 1=SingleText, 2=BestText, 3= SingleImage, 4= BestImage.
   **/
  @ApiModelProperty(value = "The poll type Id value can be 1=SingleText, 2=BestText, 3= SingleImage, 4= BestImage.")
  public Long getPollTypeId() {
    return pollTypeId;
  }
  public void setPollTypeId(Long pollTypeId) {
    this.pollTypeId = pollTypeId;
  }

  /**
   * the poll create date.
   **/
  @ApiModelProperty(value = "the poll create date.")
  public Date getCreateDate() {
    return createDate;
  }
  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  /**
   * The poll status can be \"InProgress\",              \"Pending\" ,             \"Completed\",              \"Expired\" ,             \"Suspended\",              \"Draft\" ,
   **/
  @ApiModelProperty(value = "The poll status can be \"InProgress\",              \"Pending\" ,             \"Completed\",              \"Expired\" ,             \"Suspended\",              \"Draft\" ,")
  public String getPollStatus() {
    return pollStatus;
  }
  public void setPollStatus(String pollStatus) {
    this.pollStatus = pollStatus;
  }

  /**
   * the poll title.
   **/
  @ApiModelProperty(value = "the poll title.")
  public String getPollTitle() {
    return pollTitle;
  }
  public void setPollTitle(String pollTitle) {
    this.pollTitle = pollTitle;
  }

  /**
   * the question.
   **/
  @ApiModelProperty(value = "the question.")
  public String getQuestion() {
    return question;
  }
  public void setQuestion(String question) {
    this.question = question;
  }

  /**
   * the keywords.
   **/
  @ApiModelProperty(value = "the keywords.")
  public String getKeywords() {
    return keywords;
  }
  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }

  /**
   * the expiration date.
   **/
  @ApiModelProperty(value = "the expiration date.")
  public Date getExpirationDate() {
    return expirationDate;
  }
  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  /**
   * the poll overview.
   **/
  @ApiModelProperty(value = "the poll overview.")
  public String getPollOverview() {
    return pollOverview;
  }
  public void setPollOverview(String pollOverview) {
    this.pollOverview = pollOverview;
  }

  /**
   * the first option.
   **/
  @ApiModelProperty(value = "the first option.")
  public String getFirstOption() {
    return firstOption;
  }
  public void setFirstOption(String firstOption) {
    this.firstOption = firstOption;
  }

  /**
   * the second option.
   **/
  @ApiModelProperty(value = "the second option.")
  public String getSecondOption() {
    return secondOption;
  }
  public void setSecondOption(String secondOption) {
    this.secondOption = secondOption;
  }

  /**
   * the first image path.
   **/
  @ApiModelProperty(value = "the first image path.")
  public String getFirstImagePath() {
    return firstImagePath;
  }
  public void setFirstImagePath(String firstImagePath) {
    this.firstImagePath = firstImagePath;
  }

  /**
   * the second image path.
   **/
  @ApiModelProperty(value = "the second image path.")
  public String getSecondImagePath() {
    return secondImagePath;
  }
  public void setSecondImagePath(String secondImagePath) {
    this.secondImagePath = secondImagePath;
  }

  /**
   * the assignment duration in seconds.
   **/
  @ApiModelProperty(value = "the assignment duration in seconds.")
  public Long getAssignmentDurationInSeconds() {
    return assignmentDurationInSeconds;
  }
  public void setAssignmentDurationInSeconds(Long assignmentDurationInSeconds) {
    this.assignmentDurationInSeconds = assignmentDurationInSeconds;
  }

  /**
   * the maximum assignments.
   **/
  @ApiModelProperty(value = "the maximum assignments.")
  public Long getMaxAssignments() {
    return maxAssignments;
  }
  public void setMaxAssignments(Long maxAssignments) {
    this.maxAssignments = maxAssignments;
  }

  /**
   * the is published. IsPublished = false for Draft else true
   **/
  @ApiModelProperty(value = "the is published. IsPublished = false for Draft else true")
  public Boolean getIsPublished() {
    return isPublished;
  }
  public void setIsPublished(Boolean isPublished) {
    this.isPublished = isPublished;
  }

  /**
   * the life time in seconds the time of expiration from creation.
   **/
  @ApiModelProperty(value = "the life time in seconds the time of expiration from creation.")
  public Long getLifeTimeInSeconds() {
    return lifeTimeInSeconds;
  }
  public void setLifeTimeInSeconds(Long lifeTimeInSeconds) {
    this.lifeTimeInSeconds = lifeTimeInSeconds;
  }

  /**
   * the choice first counter.
   **/
  @ApiModelProperty(value = "the choice first counter.")
  public Long getChoiceFirstCounter() {
    return choiceFirstCounter;
  }
  public void setChoiceFirstCounter(Long choiceFirstCounter) {
    this.choiceFirstCounter = choiceFirstCounter;
  }

  /**
   * the choice second counter.
   **/
  @ApiModelProperty(value = "the choice second counter.")
  public Long getChoiceSecondCounter() {
    return choiceSecondCounter;
  }
  public void setChoiceSecondCounter(Long choiceSecondCounter) {
    this.choiceSecondCounter = choiceSecondCounter;
  }

  /**
   * Result Counter
   **/
  @ApiModelProperty(value = "Result Counter")
  public Long getResultCounter() {
    return resultCounter;
  }
  public void setResultCounter(Long resultCounter) {
    this.resultCounter = resultCounter;
  }

  /**
   * true if this instance is filter option; otherwise, false.
   **/
  @ApiModelProperty(value = "true if this instance is filter option; otherwise, false.")
  public Boolean getIsFilterOption() {
    return isFilterOption;
  }
  public void setIsFilterOption(Boolean isFilterOption) {
    this.isFilterOption = isFilterOption;
  }

  /**
   * the filter main category.
   **/
  @ApiModelProperty(value = "the filter main category.")
  public String getFilterMainCategory() {
    return filterMainCategory;
  }
  public void setFilterMainCategory(String filterMainCategory) {
    this.filterMainCategory = filterMainCategory;
  }

  /**
   * the filters json.
   **/
  @ApiModelProperty(value = "the filters json.")
  public String getFiltersJson() {
    return filtersJson;
  }
  public void setFiltersJson(String filtersJson) {
    this.filtersJson = filtersJson;
  }

  /**
   * a value indicating whether [enable public].
   **/
  @ApiModelProperty(value = "a value indicating whether [enable public].")
  public Boolean getIsEnablePublic() {
    return isEnablePublic;
  }
  public void setIsEnablePublic(Boolean isEnablePublic) {
    this.isEnablePublic = isEnablePublic;
  }

  /**
   * the identifier cat.
   **/
  @ApiModelProperty(value = "the identifier cat.")
  public Long getCatId() {
    return catId;
  }
  public void setCatId(Long catId) {
    this.catId = catId;
  }

  /**
   * the name of the category.
   **/
  @ApiModelProperty(value = "the name of the category.")
  public String getCatName() {
    return catName;
  }
  public void setCatName(String catName) {
    this.catName = catName;
  }

  /**
   * the name of the user.
   **/
  @ApiModelProperty(value = "the name of the user.")
  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }

  /**
   * the owner identifier.
   **/
  @ApiModelProperty(value = "the owner identifier.")
  public UUID getOwnerId() {
    return ownerId;
  }
  public void setOwnerId(UUID ownerId) {
    this.ownerId = ownerId;
  }

  /**
   * the total count.
   **/
  @ApiModelProperty(value = "the total count.")
  public Long getTotalCount() {
    return totalCount;
  }
  public void setTotalCount(Long totalCount) {
    this.totalCount = totalCount;
  }

  /**
   * the is adult.
   **/
  @ApiModelProperty(value = "the is adult.")
  public Boolean getIsAdult() {
    return isAdult;
  }
  public void setIsAdult(Boolean isAdult) {
    this.isAdult = isAdult;
  }

  /**
   * the completed on date.
   **/
  @ApiModelProperty(value = "the completed on date.")
  public Date getCompletedOn() {
    return completedOn;
  }
  public void setCompletedOn(Date completedOn) {
    this.completedOn = completedOn;
  }

  /**
   * 
   **/
  @ApiModelProperty(value = "")
  public String getTags() {
    return tags;
  }
  public void setTags(String tags) {
    this.tags = tags;
  }

  /**
   * The alllowed anonymously
   **/
  @ApiModelProperty(value = "The alllowed anonymously")
  public Integer getAlllowedAnonymously() {
    return alllowedAnonymously;
  }
  public void setAlllowedAnonymously(Integer alllowedAnonymously) {
    this.alllowedAnonymously = alllowedAnonymously;
  }

  /**
   * The is enabled comments
   **/
  @ApiModelProperty(value = "The is enabled comments")
  public Boolean getIsEnabledComments() {
    return isEnabledComments;
  }
  public void setIsEnabledComments(Boolean isEnabledComments) {
    this.isEnabledComments = isEnabledComments;
  }

  /**
   * The demographic completed
   **/
  @ApiModelProperty(value = "The demographic completed")
  public Long getDemographicCompleted() {
    return demographicCompleted;
  }
  public void setDemographicCompleted(Long demographicCompleted) {
    this.demographicCompleted = demographicCompleted;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ListPollModel listPollModel = (ListPollModel) o;
    return (this.pollId == null ? listPollModel.pollId == null : this.pollId.equals(listPollModel.pollId)) &&
        (this.hitId == null ? listPollModel.hitId == null : this.hitId.equals(listPollModel.hitId)) &&
        (this.pollTypeId == null ? listPollModel.pollTypeId == null : this.pollTypeId.equals(listPollModel.pollTypeId)) &&
        (this.createDate == null ? listPollModel.createDate == null : this.createDate.equals(listPollModel.createDate)) &&
        (this.pollStatus == null ? listPollModel.pollStatus == null : this.pollStatus.equals(listPollModel.pollStatus)) &&
        (this.pollTitle == null ? listPollModel.pollTitle == null : this.pollTitle.equals(listPollModel.pollTitle)) &&
        (this.question == null ? listPollModel.question == null : this.question.equals(listPollModel.question)) &&
        (this.keywords == null ? listPollModel.keywords == null : this.keywords.equals(listPollModel.keywords)) &&
        (this.expirationDate == null ? listPollModel.expirationDate == null : this.expirationDate.equals(listPollModel.expirationDate)) &&
        (this.pollOverview == null ? listPollModel.pollOverview == null : this.pollOverview.equals(listPollModel.pollOverview)) &&
        (this.firstOption == null ? listPollModel.firstOption == null : this.firstOption.equals(listPollModel.firstOption)) &&
        (this.secondOption == null ? listPollModel.secondOption == null : this.secondOption.equals(listPollModel.secondOption)) &&
        (this.firstImagePath == null ? listPollModel.firstImagePath == null : this.firstImagePath.equals(listPollModel.firstImagePath)) &&
        (this.secondImagePath == null ? listPollModel.secondImagePath == null : this.secondImagePath.equals(listPollModel.secondImagePath)) &&
        (this.assignmentDurationInSeconds == null ? listPollModel.assignmentDurationInSeconds == null : this.assignmentDurationInSeconds.equals(listPollModel.assignmentDurationInSeconds)) &&
        (this.maxAssignments == null ? listPollModel.maxAssignments == null : this.maxAssignments.equals(listPollModel.maxAssignments)) &&
        (this.isPublished == null ? listPollModel.isPublished == null : this.isPublished.equals(listPollModel.isPublished)) &&
        (this.lifeTimeInSeconds == null ? listPollModel.lifeTimeInSeconds == null : this.lifeTimeInSeconds.equals(listPollModel.lifeTimeInSeconds)) &&
        (this.choiceFirstCounter == null ? listPollModel.choiceFirstCounter == null : this.choiceFirstCounter.equals(listPollModel.choiceFirstCounter)) &&
        (this.choiceSecondCounter == null ? listPollModel.choiceSecondCounter == null : this.choiceSecondCounter.equals(listPollModel.choiceSecondCounter)) &&
        (this.resultCounter == null ? listPollModel.resultCounter == null : this.resultCounter.equals(listPollModel.resultCounter)) &&
        (this.isFilterOption == null ? listPollModel.isFilterOption == null : this.isFilterOption.equals(listPollModel.isFilterOption)) &&
        (this.filterMainCategory == null ? listPollModel.filterMainCategory == null : this.filterMainCategory.equals(listPollModel.filterMainCategory)) &&
        (this.filtersJson == null ? listPollModel.filtersJson == null : this.filtersJson.equals(listPollModel.filtersJson)) &&
        (this.isEnablePublic == null ? listPollModel.isEnablePublic == null : this.isEnablePublic.equals(listPollModel.isEnablePublic)) &&
        (this.catId == null ? listPollModel.catId == null : this.catId.equals(listPollModel.catId)) &&
        (this.catName == null ? listPollModel.catName == null : this.catName.equals(listPollModel.catName)) &&
        (this.userName == null ? listPollModel.userName == null : this.userName.equals(listPollModel.userName)) &&
        (this.ownerId == null ? listPollModel.ownerId == null : this.ownerId.equals(listPollModel.ownerId)) &&
        (this.totalCount == null ? listPollModel.totalCount == null : this.totalCount.equals(listPollModel.totalCount)) &&
        (this.isAdult == null ? listPollModel.isAdult == null : this.isAdult.equals(listPollModel.isAdult)) &&
        (this.completedOn == null ? listPollModel.completedOn == null : this.completedOn.equals(listPollModel.completedOn)) &&
        (this.tags == null ? listPollModel.tags == null : this.tags.equals(listPollModel.tags)) &&
        (this.alllowedAnonymously == null ? listPollModel.alllowedAnonymously == null : this.alllowedAnonymously.equals(listPollModel.alllowedAnonymously)) &&
        (this.isEnabledComments == null ? listPollModel.isEnabledComments == null : this.isEnabledComments.equals(listPollModel.isEnabledComments)) &&
        (this.demographicCompleted == null ? listPollModel.demographicCompleted == null : this.demographicCompleted.equals(listPollModel.demographicCompleted));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.pollId == null ? 0: this.pollId.hashCode());
    result = 31 * result + (this.hitId == null ? 0: this.hitId.hashCode());
    result = 31 * result + (this.pollTypeId == null ? 0: this.pollTypeId.hashCode());
    result = 31 * result + (this.createDate == null ? 0: this.createDate.hashCode());
    result = 31 * result + (this.pollStatus == null ? 0: this.pollStatus.hashCode());
    result = 31 * result + (this.pollTitle == null ? 0: this.pollTitle.hashCode());
    result = 31 * result + (this.question == null ? 0: this.question.hashCode());
    result = 31 * result + (this.keywords == null ? 0: this.keywords.hashCode());
    result = 31 * result + (this.expirationDate == null ? 0: this.expirationDate.hashCode());
    result = 31 * result + (this.pollOverview == null ? 0: this.pollOverview.hashCode());
    result = 31 * result + (this.firstOption == null ? 0: this.firstOption.hashCode());
    result = 31 * result + (this.secondOption == null ? 0: this.secondOption.hashCode());
    result = 31 * result + (this.firstImagePath == null ? 0: this.firstImagePath.hashCode());
    result = 31 * result + (this.secondImagePath == null ? 0: this.secondImagePath.hashCode());
    result = 31 * result + (this.assignmentDurationInSeconds == null ? 0: this.assignmentDurationInSeconds.hashCode());
    result = 31 * result + (this.maxAssignments == null ? 0: this.maxAssignments.hashCode());
    result = 31 * result + (this.isPublished == null ? 0: this.isPublished.hashCode());
    result = 31 * result + (this.lifeTimeInSeconds == null ? 0: this.lifeTimeInSeconds.hashCode());
    result = 31 * result + (this.choiceFirstCounter == null ? 0: this.choiceFirstCounter.hashCode());
    result = 31 * result + (this.choiceSecondCounter == null ? 0: this.choiceSecondCounter.hashCode());
    result = 31 * result + (this.resultCounter == null ? 0: this.resultCounter.hashCode());
    result = 31 * result + (this.isFilterOption == null ? 0: this.isFilterOption.hashCode());
    result = 31 * result + (this.filterMainCategory == null ? 0: this.filterMainCategory.hashCode());
    result = 31 * result + (this.filtersJson == null ? 0: this.filtersJson.hashCode());
    result = 31 * result + (this.isEnablePublic == null ? 0: this.isEnablePublic.hashCode());
    result = 31 * result + (this.catId == null ? 0: this.catId.hashCode());
    result = 31 * result + (this.catName == null ? 0: this.catName.hashCode());
    result = 31 * result + (this.userName == null ? 0: this.userName.hashCode());
    result = 31 * result + (this.ownerId == null ? 0: this.ownerId.hashCode());
    result = 31 * result + (this.totalCount == null ? 0: this.totalCount.hashCode());
    result = 31 * result + (this.isAdult == null ? 0: this.isAdult.hashCode());
    result = 31 * result + (this.completedOn == null ? 0: this.completedOn.hashCode());
    result = 31 * result + (this.tags == null ? 0: this.tags.hashCode());
    result = 31 * result + (this.alllowedAnonymously == null ? 0: this.alllowedAnonymously.hashCode());
    result = 31 * result + (this.isEnabledComments == null ? 0: this.isEnabledComments.hashCode());
    result = 31 * result + (this.demographicCompleted == null ? 0: this.demographicCompleted.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ListPollModel {\n");
    
    sb.append("  pollId: ").append(pollId).append("\n");
    sb.append("  hitId: ").append(hitId).append("\n");
    sb.append("  pollTypeId: ").append(pollTypeId).append("\n");
    sb.append("  createDate: ").append(createDate).append("\n");
    sb.append("  pollStatus: ").append(pollStatus).append("\n");
    sb.append("  pollTitle: ").append(pollTitle).append("\n");
    sb.append("  question: ").append(question).append("\n");
    sb.append("  keywords: ").append(keywords).append("\n");
    sb.append("  expirationDate: ").append(expirationDate).append("\n");
    sb.append("  pollOverview: ").append(pollOverview).append("\n");
    sb.append("  firstOption: ").append(firstOption).append("\n");
    sb.append("  secondOption: ").append(secondOption).append("\n");
    sb.append("  firstImagePath: ").append(firstImagePath).append("\n");
    sb.append("  secondImagePath: ").append(secondImagePath).append("\n");
    sb.append("  assignmentDurationInSeconds: ").append(assignmentDurationInSeconds).append("\n");
    sb.append("  maxAssignments: ").append(maxAssignments).append("\n");
    sb.append("  isPublished: ").append(isPublished).append("\n");
    sb.append("  lifeTimeInSeconds: ").append(lifeTimeInSeconds).append("\n");
    sb.append("  choiceFirstCounter: ").append(choiceFirstCounter).append("\n");
    sb.append("  choiceSecondCounter: ").append(choiceSecondCounter).append("\n");
    sb.append("  resultCounter: ").append(resultCounter).append("\n");
    sb.append("  isFilterOption: ").append(isFilterOption).append("\n");
    sb.append("  filterMainCategory: ").append(filterMainCategory).append("\n");
    sb.append("  filtersJson: ").append(filtersJson).append("\n");
    sb.append("  isEnablePublic: ").append(isEnablePublic).append("\n");
    sb.append("  catId: ").append(catId).append("\n");
    sb.append("  catName: ").append(catName).append("\n");
    sb.append("  userName: ").append(userName).append("\n");
    sb.append("  ownerId: ").append(ownerId).append("\n");
    sb.append("  totalCount: ").append(totalCount).append("\n");
    sb.append("  isAdult: ").append(isAdult).append("\n");
    sb.append("  completedOn: ").append(completedOn).append("\n");
    sb.append("  tags: ").append(tags).append("\n");
    sb.append("  alllowedAnonymously: ").append(alllowedAnonymously).append("\n");
    sb.append("  isEnabledComments: ").append(isEnabledComments).append("\n");
    sb.append("  demographicCompleted: ").append(demographicCompleted).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
