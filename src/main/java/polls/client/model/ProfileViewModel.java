/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import java.util.*;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * Profile View Model
 **/
@ApiModel(description = "Profile View Model")
public class ProfileViewModel {
  
  @SerializedName("userId")
  private UUID userId = null;
  @SerializedName("companyName")
  private String companyName = null;
  @SerializedName("jobTitle")
  private String jobTitle = null;
  @SerializedName("countryCode")
  private Long countryCode = null;
  @SerializedName("phoneNumber")
  private String phoneNumber = null;
  @SerializedName("gender")
  private String gender = null;
  @SerializedName("birthDate")
  private Date birthDate = null;
  @SerializedName("educationLevel")
  private String educationLevel = null;
  @SerializedName("addressLine1")
  private String addressLine1 = null;
  @SerializedName("addressLine2")
  private String addressLine2 = null;
  @SerializedName("addressCity")
  private String addressCity = null;
  @SerializedName("addressCountry")
  private String addressCountry = null;
  @SerializedName("addressState")
  private String addressState = null;
  @SerializedName("addressZip")
  private Long addressZip = null;
  @SerializedName("ownerId")
  private UUID ownerId = null;
  @SerializedName("pictureUrl")
  private String pictureUrl = null;
  @SerializedName("profilePictures")
  private Map profilePictures = null;
  @SerializedName("isEmailVerified")
  private Boolean isEmailVerified = null;
  @SerializedName("isPhoneVerified")
  private Boolean isPhoneVerified = null;
  @SerializedName("publicPolls")
  private Long publicPolls = null;
  @SerializedName("businessPolls")
  private Long businessPolls = null;
  @SerializedName("privatePolls")
  private Long privatePolls = null;
  @SerializedName("totalPolls")
  private Long totalPolls = null;
  @SerializedName("deletedPictures")
  private List<UUID> deletedPictures = null;
  @SerializedName("lastLogin")
  private Date lastLogin = null;
  @SerializedName("memberSince")
  private Date memberSince = null;
  @SerializedName("firstName")
  private String firstName = null;
  @SerializedName("lastName")
  private String lastName = null;
  @SerializedName("payPalEmail")
  private String payPalEmail = null;
  @SerializedName("id")
  private UUID id = null;
  @SerializedName("relationshipStatus")
  private String relationshipStatus = null;
  @SerializedName("maritalStatus")
  private String maritalStatus = null;
  @SerializedName("pictureId")
  private UUID pictureId = null;
  @SerializedName("isPublicProfile")
  private Boolean isPublicProfile = null;
  @SerializedName("isPhonePublic")
  private Boolean isPhonePublic = null;
  @SerializedName("isEmailPublic")
  private Boolean isEmailPublic = null;
  @SerializedName("viewCounter")
  private Long viewCounter = null;
  @SerializedName("phoneType")
  private Integer phoneType = null;

  /**
   * Gets or sets the user identifier.
   **/
  @ApiModelProperty(value = "Gets or sets the user identifier.")
  public UUID getUserId() {
    return userId;
  }
  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  /**
   * The company name
   **/
  @ApiModelProperty(required = true, value = "The company name")
  public String getCompanyName() {
    return companyName;
  }
  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  /**
   * The job title
   **/
  @ApiModelProperty(required = true, value = "The job title")
  public String getJobTitle() {
    return jobTitle;
  }
  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  /**
   * Gets or sets the country code.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the country code.")
  public Long getCountryCode() {
    return countryCode;
  }
  public void setCountryCode(Long countryCode) {
    this.countryCode = countryCode;
  }

  /**
   * The phone number
   **/
  @ApiModelProperty(required = true, value = "The phone number")
  public String getPhoneNumber() {
    return phoneNumber;
  }
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * The gender Male/Female or M or F
   **/
  @ApiModelProperty(required = true, value = "The gender Male/Female or M or F")
  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }

  /**
   * The birth date Format {0:yyyy-MM-dd}
   **/
  @ApiModelProperty(required = true, value = "The birth date Format {0:yyyy-MM-dd}")
  public Date getBirthDate() {
    return birthDate;
  }
  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  /**
   * The education level Values are College, Associates Degree, Bachelors /Degree, Graduate Degree, Masters Degree, Doctoral Degree (PHD), Trade/Technical/Vocational
   **/
  @ApiModelProperty(required = true, value = "The education level Values are College, Associates Degree, Bachelors /Degree, Graduate Degree, Masters Degree, Doctoral Degree (PHD), Trade/Technical/Vocational")
  public String getEducationLevel() {
    return educationLevel;
  }
  public void setEducationLevel(String educationLevel) {
    this.educationLevel = educationLevel;
  }

  /**
   * Gets or sets the address line1.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the address line1.")
  public String getAddressLine1() {
    return addressLine1;
  }
  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  /**
   * Gets or sets the address line2.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the address line2.")
  public String getAddressLine2() {
    return addressLine2;
  }
  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  /**
   * Gets or sets the address city.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the address city.")
  public String getAddressCity() {
    return addressCity;
  }
  public void setAddressCity(String addressCity) {
    this.addressCity = addressCity;
  }

  /**
   * Gets or sets the address country.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the address country.")
  public String getAddressCountry() {
    return addressCountry;
  }
  public void setAddressCountry(String addressCountry) {
    this.addressCountry = addressCountry;
  }

  /**
   * Gets or sets the state of the address.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the state of the address.")
  public String getAddressState() {
    return addressState;
  }
  public void setAddressState(String addressState) {
    this.addressState = addressState;
  }

  /**
   * Gets or sets the address zipCode.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the address zipCode.")
  public Long getAddressZip() {
    return addressZip;
  }
  public void setAddressZip(Long addressZip) {
    this.addressZip = addressZip;
  }

  /**
   * The owner identifier for owner account it is userId
   **/
  @ApiModelProperty(value = "The owner identifier for owner account it is userId")
  public UUID getOwnerId() {
    return ownerId;
  }
  public void setOwnerId(UUID ownerId) {
    this.ownerId = ownerId;
  }

  /**
   * Gets or sets the picture URL.
   **/
  @ApiModelProperty(value = "Gets or sets the picture URL.")
  public String getPictureUrl() {
    return pictureUrl;
  }
  public void setPictureUrl(String pictureUrl) {
    this.pictureUrl = pictureUrl;
  }

  /**
   * Gets or sets the profile pictures.
   **/
  @ApiModelProperty(value = "Gets or sets the profile pictures.")
  public Map getProfilePictures() {
    return profilePictures;
  }
  public void setProfilePictures(Map profilePictures) {
    this.profilePictures = profilePictures;
  }

  /**
   * Gets a value indicating whether this instance is email verified.
   **/
  @ApiModelProperty(value = "Gets a value indicating whether this instance is email verified.")
  public Boolean getIsEmailVerified() {
    return isEmailVerified;
  }
  public void setIsEmailVerified(Boolean isEmailVerified) {
    this.isEmailVerified = isEmailVerified;
  }

  /**
   * Gets a value indicating whether this instance is phone verified.
   **/
  @ApiModelProperty(value = "Gets a value indicating whether this instance is phone verified.")
  public Boolean getIsPhoneVerified() {
    return isPhoneVerified;
  }
  public void setIsPhoneVerified(Boolean isPhoneVerified) {
    this.isPhoneVerified = isPhoneVerified;
  }

  /**
   * Gets or sets the public polls.
   **/
  @ApiModelProperty(value = "Gets or sets the public polls.")
  public Long getPublicPolls() {
    return publicPolls;
  }
  public void setPublicPolls(Long publicPolls) {
    this.publicPolls = publicPolls;
  }

  /**
   * Gets or sets the business polls.
   **/
  @ApiModelProperty(value = "Gets or sets the business polls.")
  public Long getBusinessPolls() {
    return businessPolls;
  }
  public void setBusinessPolls(Long businessPolls) {
    this.businessPolls = businessPolls;
  }

  /**
   * Gets or sets the private polls.
   **/
  @ApiModelProperty(value = "Gets or sets the private polls.")
  public Long getPrivatePolls() {
    return privatePolls;
  }
  public void setPrivatePolls(Long privatePolls) {
    this.privatePolls = privatePolls;
  }

  /**
   * Gets or sets the total polls.
   **/
  @ApiModelProperty(value = "Gets or sets the total polls.")
  public Long getTotalPolls() {
    return totalPolls;
  }
  public void setTotalPolls(Long totalPolls) {
    this.totalPolls = totalPolls;
  }

  /**
   * Gets or sets the deleted pics array.
   **/
  @ApiModelProperty(value = "Gets or sets the deleted pics array.")
  public List<UUID> getDeletedPictures() {
    return deletedPictures;
  }
  public void setDeletedPictures(List<UUID> deletedPictures) {
    this.deletedPictures = deletedPictures;
  }

  /**
   * Get the last login.
   **/
  @ApiModelProperty(value = "Get the last login.")
  public Date getLastLogin() {
    return lastLogin;
  }
  public void setLastLogin(Date lastLogin) {
    this.lastLogin = lastLogin;
  }

  /**
   * Gets the member since.
   **/
  @ApiModelProperty(value = "Gets the member since.")
  public Date getMemberSince() {
    return memberSince;
  }
  public void setMemberSince(Date memberSince) {
    this.memberSince = memberSince;
  }

  /**
   * Gets or sets the first name.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the first name.")
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * The last name
   **/
  @ApiModelProperty(required = true, value = "The last name")
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * The paypal email
   **/
  @ApiModelProperty(required = true, value = "The paypal email")
  public String getPayPalEmail() {
    return payPalEmail;
  }
  public void setPayPalEmail(String payPalEmail) {
    this.payPalEmail = payPalEmail;
  }

  /**
   * The identifier guid for the contact
   **/
  @ApiModelProperty(value = "The identifier guid for the contact")
  public UUID getId() {
    return id;
  }
  public void setId(UUID id) {
    this.id = id;
  }

  /**
   * The relationship status Pending, Accepted,None Etc.
   **/
  @ApiModelProperty(value = "The relationship status Pending, Accepted,None Etc.")
  public String getRelationshipStatus() {
    return relationshipStatus;
  }
  public void setRelationshipStatus(String relationshipStatus) {
    this.relationshipStatus = relationshipStatus;
  }

  /**
   * The marital status Married, Single, In Relationship Etc.
   **/
  @ApiModelProperty(value = "The marital status Married, Single, In Relationship Etc.")
  public String getMaritalStatus() {
    return maritalStatus;
  }
  public void setMaritalStatus(String maritalStatus) {
    this.maritalStatus = maritalStatus;
  }

  /**
   * The picture identifier, it is used to update the picture info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.
   **/
  @ApiModelProperty(value = "The picture identifier, it is used to update the picture info, and for retrieve filename, you need to remove all (-) and add file extention to .png all files on server are saved into png format.")
  public UUID getPictureId() {
    return pictureId;
  }
  public void setPictureId(UUID pictureId) {
    this.pictureId = pictureId;
  }

  /**
   * The profile is public              value is true else false
   **/
  @ApiModelProperty(value = "The profile is public              value is true else false")
  public Boolean getIsPublicProfile() {
    return isPublicProfile;
  }
  public void setIsPublicProfile(Boolean isPublicProfile) {
    this.isPublicProfile = isPublicProfile;
  }

  /**
   * The  phone is public             value is true else false
   **/
  @ApiModelProperty(value = "The  phone is public             value is true else false")
  public Boolean getIsPhonePublic() {
    return isPhonePublic;
  }
  public void setIsPhonePublic(Boolean isPhonePublic) {
    this.isPhonePublic = isPhonePublic;
  }

  /**
   * The  email is public             value is true else false
   **/
  @ApiModelProperty(value = "The  email is public             value is true else false")
  public Boolean getIsEmailPublic() {
    return isEmailPublic;
  }
  public void setIsEmailPublic(Boolean isEmailPublic) {
    this.isEmailPublic = isEmailPublic;
  }

  /**
   * The view counter for the public profile
   **/
  @ApiModelProperty(value = "The view counter for the public profile")
  public Long getViewCounter() {
    return viewCounter;
  }
  public void setViewCounter(Long viewCounter) {
    this.viewCounter = viewCounter;
  }

  /**
   * Gets or sets the type of the phone.
   **/
  @ApiModelProperty(value = "Gets or sets the type of the phone.")
  public Integer getPhoneType() {
    return phoneType;
  }
  public void setPhoneType(Integer phoneType) {
    this.phoneType = phoneType;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProfileViewModel profileViewModel = (ProfileViewModel) o;
    return (this.userId == null ? profileViewModel.userId == null : this.userId.equals(profileViewModel.userId)) &&
        (this.companyName == null ? profileViewModel.companyName == null : this.companyName.equals(profileViewModel.companyName)) &&
        (this.jobTitle == null ? profileViewModel.jobTitle == null : this.jobTitle.equals(profileViewModel.jobTitle)) &&
        (this.countryCode == null ? profileViewModel.countryCode == null : this.countryCode.equals(profileViewModel.countryCode)) &&
        (this.phoneNumber == null ? profileViewModel.phoneNumber == null : this.phoneNumber.equals(profileViewModel.phoneNumber)) &&
        (this.gender == null ? profileViewModel.gender == null : this.gender.equals(profileViewModel.gender)) &&
        (this.birthDate == null ? profileViewModel.birthDate == null : this.birthDate.equals(profileViewModel.birthDate)) &&
        (this.educationLevel == null ? profileViewModel.educationLevel == null : this.educationLevel.equals(profileViewModel.educationLevel)) &&
        (this.addressLine1 == null ? profileViewModel.addressLine1 == null : this.addressLine1.equals(profileViewModel.addressLine1)) &&
        (this.addressLine2 == null ? profileViewModel.addressLine2 == null : this.addressLine2.equals(profileViewModel.addressLine2)) &&
        (this.addressCity == null ? profileViewModel.addressCity == null : this.addressCity.equals(profileViewModel.addressCity)) &&
        (this.addressCountry == null ? profileViewModel.addressCountry == null : this.addressCountry.equals(profileViewModel.addressCountry)) &&
        (this.addressState == null ? profileViewModel.addressState == null : this.addressState.equals(profileViewModel.addressState)) &&
        (this.addressZip == null ? profileViewModel.addressZip == null : this.addressZip.equals(profileViewModel.addressZip)) &&
        (this.ownerId == null ? profileViewModel.ownerId == null : this.ownerId.equals(profileViewModel.ownerId)) &&
        (this.pictureUrl == null ? profileViewModel.pictureUrl == null : this.pictureUrl.equals(profileViewModel.pictureUrl)) &&
        (this.profilePictures == null ? profileViewModel.profilePictures == null : this.profilePictures.equals(profileViewModel.profilePictures)) &&
        (this.isEmailVerified == null ? profileViewModel.isEmailVerified == null : this.isEmailVerified.equals(profileViewModel.isEmailVerified)) &&
        (this.isPhoneVerified == null ? profileViewModel.isPhoneVerified == null : this.isPhoneVerified.equals(profileViewModel.isPhoneVerified)) &&
        (this.publicPolls == null ? profileViewModel.publicPolls == null : this.publicPolls.equals(profileViewModel.publicPolls)) &&
        (this.businessPolls == null ? profileViewModel.businessPolls == null : this.businessPolls.equals(profileViewModel.businessPolls)) &&
        (this.privatePolls == null ? profileViewModel.privatePolls == null : this.privatePolls.equals(profileViewModel.privatePolls)) &&
        (this.totalPolls == null ? profileViewModel.totalPolls == null : this.totalPolls.equals(profileViewModel.totalPolls)) &&
        (this.deletedPictures == null ? profileViewModel.deletedPictures == null : this.deletedPictures.equals(profileViewModel.deletedPictures)) &&
        (this.lastLogin == null ? profileViewModel.lastLogin == null : this.lastLogin.equals(profileViewModel.lastLogin)) &&
        (this.memberSince == null ? profileViewModel.memberSince == null : this.memberSince.equals(profileViewModel.memberSince)) &&
        (this.firstName == null ? profileViewModel.firstName == null : this.firstName.equals(profileViewModel.firstName)) &&
        (this.lastName == null ? profileViewModel.lastName == null : this.lastName.equals(profileViewModel.lastName)) &&
        (this.payPalEmail == null ? profileViewModel.payPalEmail == null : this.payPalEmail.equals(profileViewModel.payPalEmail)) &&
        (this.id == null ? profileViewModel.id == null : this.id.equals(profileViewModel.id)) &&
        (this.relationshipStatus == null ? profileViewModel.relationshipStatus == null : this.relationshipStatus.equals(profileViewModel.relationshipStatus)) &&
        (this.maritalStatus == null ? profileViewModel.maritalStatus == null : this.maritalStatus.equals(profileViewModel.maritalStatus)) &&
        (this.pictureId == null ? profileViewModel.pictureId == null : this.pictureId.equals(profileViewModel.pictureId)) &&
        (this.isPublicProfile == null ? profileViewModel.isPublicProfile == null : this.isPublicProfile.equals(profileViewModel.isPublicProfile)) &&
        (this.isPhonePublic == null ? profileViewModel.isPhonePublic == null : this.isPhonePublic.equals(profileViewModel.isPhonePublic)) &&
        (this.isEmailPublic == null ? profileViewModel.isEmailPublic == null : this.isEmailPublic.equals(profileViewModel.isEmailPublic)) &&
        (this.viewCounter == null ? profileViewModel.viewCounter == null : this.viewCounter.equals(profileViewModel.viewCounter)) &&
        (this.phoneType == null ? profileViewModel.phoneType == null : this.phoneType.equals(profileViewModel.phoneType));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.userId == null ? 0: this.userId.hashCode());
    result = 31 * result + (this.companyName == null ? 0: this.companyName.hashCode());
    result = 31 * result + (this.jobTitle == null ? 0: this.jobTitle.hashCode());
    result = 31 * result + (this.countryCode == null ? 0: this.countryCode.hashCode());
    result = 31 * result + (this.phoneNumber == null ? 0: this.phoneNumber.hashCode());
    result = 31 * result + (this.gender == null ? 0: this.gender.hashCode());
    result = 31 * result + (this.birthDate == null ? 0: this.birthDate.hashCode());
    result = 31 * result + (this.educationLevel == null ? 0: this.educationLevel.hashCode());
    result = 31 * result + (this.addressLine1 == null ? 0: this.addressLine1.hashCode());
    result = 31 * result + (this.addressLine2 == null ? 0: this.addressLine2.hashCode());
    result = 31 * result + (this.addressCity == null ? 0: this.addressCity.hashCode());
    result = 31 * result + (this.addressCountry == null ? 0: this.addressCountry.hashCode());
    result = 31 * result + (this.addressState == null ? 0: this.addressState.hashCode());
    result = 31 * result + (this.addressZip == null ? 0: this.addressZip.hashCode());
    result = 31 * result + (this.ownerId == null ? 0: this.ownerId.hashCode());
    result = 31 * result + (this.pictureUrl == null ? 0: this.pictureUrl.hashCode());
    result = 31 * result + (this.profilePictures == null ? 0: this.profilePictures.hashCode());
    result = 31 * result + (this.isEmailVerified == null ? 0: this.isEmailVerified.hashCode());
    result = 31 * result + (this.isPhoneVerified == null ? 0: this.isPhoneVerified.hashCode());
    result = 31 * result + (this.publicPolls == null ? 0: this.publicPolls.hashCode());
    result = 31 * result + (this.businessPolls == null ? 0: this.businessPolls.hashCode());
    result = 31 * result + (this.privatePolls == null ? 0: this.privatePolls.hashCode());
    result = 31 * result + (this.totalPolls == null ? 0: this.totalPolls.hashCode());
    result = 31 * result + (this.deletedPictures == null ? 0: this.deletedPictures.hashCode());
    result = 31 * result + (this.lastLogin == null ? 0: this.lastLogin.hashCode());
    result = 31 * result + (this.memberSince == null ? 0: this.memberSince.hashCode());
    result = 31 * result + (this.firstName == null ? 0: this.firstName.hashCode());
    result = 31 * result + (this.lastName == null ? 0: this.lastName.hashCode());
    result = 31 * result + (this.payPalEmail == null ? 0: this.payPalEmail.hashCode());
    result = 31 * result + (this.id == null ? 0: this.id.hashCode());
    result = 31 * result + (this.relationshipStatus == null ? 0: this.relationshipStatus.hashCode());
    result = 31 * result + (this.maritalStatus == null ? 0: this.maritalStatus.hashCode());
    result = 31 * result + (this.pictureId == null ? 0: this.pictureId.hashCode());
    result = 31 * result + (this.isPublicProfile == null ? 0: this.isPublicProfile.hashCode());
    result = 31 * result + (this.isPhonePublic == null ? 0: this.isPhonePublic.hashCode());
    result = 31 * result + (this.isEmailPublic == null ? 0: this.isEmailPublic.hashCode());
    result = 31 * result + (this.viewCounter == null ? 0: this.viewCounter.hashCode());
    result = 31 * result + (this.phoneType == null ? 0: this.phoneType.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProfileViewModel {\n");
    
    sb.append("  userId: ").append(userId).append("\n");
    sb.append("  companyName: ").append(companyName).append("\n");
    sb.append("  jobTitle: ").append(jobTitle).append("\n");
    sb.append("  countryCode: ").append(countryCode).append("\n");
    sb.append("  phoneNumber: ").append(phoneNumber).append("\n");
    sb.append("  gender: ").append(gender).append("\n");
    sb.append("  birthDate: ").append(birthDate).append("\n");
    sb.append("  educationLevel: ").append(educationLevel).append("\n");
    sb.append("  addressLine1: ").append(addressLine1).append("\n");
    sb.append("  addressLine2: ").append(addressLine2).append("\n");
    sb.append("  addressCity: ").append(addressCity).append("\n");
    sb.append("  addressCountry: ").append(addressCountry).append("\n");
    sb.append("  addressState: ").append(addressState).append("\n");
    sb.append("  addressZip: ").append(addressZip).append("\n");
    sb.append("  ownerId: ").append(ownerId).append("\n");
    sb.append("  pictureUrl: ").append(pictureUrl).append("\n");
    sb.append("  profilePictures: ").append(profilePictures).append("\n");
    sb.append("  isEmailVerified: ").append(isEmailVerified).append("\n");
    sb.append("  isPhoneVerified: ").append(isPhoneVerified).append("\n");
    sb.append("  publicPolls: ").append(publicPolls).append("\n");
    sb.append("  businessPolls: ").append(businessPolls).append("\n");
    sb.append("  privatePolls: ").append(privatePolls).append("\n");
    sb.append("  totalPolls: ").append(totalPolls).append("\n");
    sb.append("  deletedPictures: ").append(deletedPictures).append("\n");
    sb.append("  lastLogin: ").append(lastLogin).append("\n");
    sb.append("  memberSince: ").append(memberSince).append("\n");
    sb.append("  firstName: ").append(firstName).append("\n");
    sb.append("  lastName: ").append(lastName).append("\n");
    sb.append("  payPalEmail: ").append(payPalEmail).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("  relationshipStatus: ").append(relationshipStatus).append("\n");
    sb.append("  maritalStatus: ").append(maritalStatus).append("\n");
    sb.append("  pictureId: ").append(pictureId).append("\n");
    sb.append("  isPublicProfile: ").append(isPublicProfile).append("\n");
    sb.append("  isPhonePublic: ").append(isPhonePublic).append("\n");
    sb.append("  isEmailPublic: ").append(isEmailPublic).append("\n");
    sb.append("  viewCounter: ").append(viewCounter).append("\n");
    sb.append("  phoneType: ").append(phoneType).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
