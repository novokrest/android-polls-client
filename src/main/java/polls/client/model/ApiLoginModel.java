/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import java.util.UUID;
import polls.client.model.DeviceModel;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * API Login Model
 **/
@ApiModel(description = "API Login Model")
public class ApiLoginModel {
  
  @SerializedName("userName")
  private String userName = null;
  @SerializedName("password")
  private String password = null;
  @SerializedName("newDevice")
  private DeviceModel newDevice = null;
  @SerializedName("deviceId")
  private UUID deviceId = null;
  @SerializedName("isPartner")
  private Boolean isPartner = null;

  /**
   * The username
   **/
  @ApiModelProperty(required = true, value = "The username")
  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }

  /**
   * The password
   **/
  @ApiModelProperty(required = true, value = "The password")
  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * the new device.
   **/
  @ApiModelProperty(required = true, value = "the new device.")
  public DeviceModel getNewDevice() {
    return newDevice;
  }
  public void setNewDevice(DeviceModel newDevice) {
    this.newDevice = newDevice;
  }

  /**
   * The device identifier
   **/
  @ApiModelProperty(value = "The device identifier")
  public UUID getDeviceId() {
    return deviceId;
  }
  public void setDeviceId(UUID deviceId) {
    this.deviceId = deviceId;
  }

  /**
   * Optiional a value indicating whether this instance is partner.
   **/
  @ApiModelProperty(value = "Optiional a value indicating whether this instance is partner.")
  public Boolean getIsPartner() {
    return isPartner;
  }
  public void setIsPartner(Boolean isPartner) {
    this.isPartner = isPartner;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiLoginModel apiLoginModel = (ApiLoginModel) o;
    return (this.userName == null ? apiLoginModel.userName == null : this.userName.equals(apiLoginModel.userName)) &&
        (this.password == null ? apiLoginModel.password == null : this.password.equals(apiLoginModel.password)) &&
        (this.newDevice == null ? apiLoginModel.newDevice == null : this.newDevice.equals(apiLoginModel.newDevice)) &&
        (this.deviceId == null ? apiLoginModel.deviceId == null : this.deviceId.equals(apiLoginModel.deviceId)) &&
        (this.isPartner == null ? apiLoginModel.isPartner == null : this.isPartner.equals(apiLoginModel.isPartner));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.userName == null ? 0: this.userName.hashCode());
    result = 31 * result + (this.password == null ? 0: this.password.hashCode());
    result = 31 * result + (this.newDevice == null ? 0: this.newDevice.hashCode());
    result = 31 * result + (this.deviceId == null ? 0: this.deviceId.hashCode());
    result = 31 * result + (this.isPartner == null ? 0: this.isPartner.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiLoginModel {\n");
    
    sb.append("  userName: ").append(userName).append("\n");
    sb.append("  password: ").append(password).append("\n");
    sb.append("  newDevice: ").append(newDevice).append("\n");
    sb.append("  deviceId: ").append(deviceId).append("\n");
    sb.append("  isPartner: ").append(isPartner).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
