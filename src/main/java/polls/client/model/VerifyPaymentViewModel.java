/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import java.util.UUID;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * Verify Payment ViewModel
 **/
@ApiModel(description = "Verify Payment ViewModel")
public class VerifyPaymentViewModel {
  
  @SerializedName("receiptData")
  private String receiptData = null;
  @SerializedName("paymentMethod")
  private String paymentMethod = null;
  @SerializedName("token")
  private String token = null;
  @SerializedName("planId")
  private UUID planId = null;
  @SerializedName("transactionId")
  private String transactionId = null;
  @SerializedName("productId")
  private String productId = null;
  @SerializedName("appType")
  private Integer appType = null;
  @SerializedName("intent")
  private String intent = null;

  /**
   * Gets or sets the receipt_data.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the receipt_data.")
  public String getReceiptData() {
    return receiptData;
  }
  public void setReceiptData(String receiptData) {
    this.receiptData = receiptData;
  }

  /**
   * Gets or sets the payment method.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the payment method.")
  public String getPaymentMethod() {
    return paymentMethod;
  }
  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  /**
   * Gets or sets the token recieved from payment gateway currently required for Google.
   **/
  @ApiModelProperty(value = "Gets or sets the token recieved from payment gateway currently required for Google.")
  public String getToken() {
    return token;
  }
  public void setToken(String token) {
    this.token = token;
  }

  /**
   * Gets or sets the system plan id.
   **/
  @ApiModelProperty(value = "Gets or sets the system plan id.")
  public UUID getPlanId() {
    return planId;
  }
  public void setPlanId(UUID planId) {
    this.planId = planId;
  }

  /**
   * Gets or sets the transaction identifier.
   **/
  @ApiModelProperty(value = "Gets or sets the transaction identifier.")
  public String getTransactionId() {
    return transactionId;
  }
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  /**
   * Gets or sets the product id for Google.
   **/
  @ApiModelProperty(value = "Gets or sets the product id for Google.")
  public String getProductId() {
    return productId;
  }
  public void setProductId(String productId) {
    this.productId = productId;
  }

  /**
   * The application type
   **/
  @ApiModelProperty(value = "The application type")
  public Integer getAppType() {
    return appType;
  }
  public void setAppType(Integer appType) {
    this.appType = appType;
  }

  /**
   * The intent
   **/
  @ApiModelProperty(value = "The intent")
  public String getIntent() {
    return intent;
  }
  public void setIntent(String intent) {
    this.intent = intent;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VerifyPaymentViewModel verifyPaymentViewModel = (VerifyPaymentViewModel) o;
    return (this.receiptData == null ? verifyPaymentViewModel.receiptData == null : this.receiptData.equals(verifyPaymentViewModel.receiptData)) &&
        (this.paymentMethod == null ? verifyPaymentViewModel.paymentMethod == null : this.paymentMethod.equals(verifyPaymentViewModel.paymentMethod)) &&
        (this.token == null ? verifyPaymentViewModel.token == null : this.token.equals(verifyPaymentViewModel.token)) &&
        (this.planId == null ? verifyPaymentViewModel.planId == null : this.planId.equals(verifyPaymentViewModel.planId)) &&
        (this.transactionId == null ? verifyPaymentViewModel.transactionId == null : this.transactionId.equals(verifyPaymentViewModel.transactionId)) &&
        (this.productId == null ? verifyPaymentViewModel.productId == null : this.productId.equals(verifyPaymentViewModel.productId)) &&
        (this.appType == null ? verifyPaymentViewModel.appType == null : this.appType.equals(verifyPaymentViewModel.appType)) &&
        (this.intent == null ? verifyPaymentViewModel.intent == null : this.intent.equals(verifyPaymentViewModel.intent));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.receiptData == null ? 0: this.receiptData.hashCode());
    result = 31 * result + (this.paymentMethod == null ? 0: this.paymentMethod.hashCode());
    result = 31 * result + (this.token == null ? 0: this.token.hashCode());
    result = 31 * result + (this.planId == null ? 0: this.planId.hashCode());
    result = 31 * result + (this.transactionId == null ? 0: this.transactionId.hashCode());
    result = 31 * result + (this.productId == null ? 0: this.productId.hashCode());
    result = 31 * result + (this.appType == null ? 0: this.appType.hashCode());
    result = 31 * result + (this.intent == null ? 0: this.intent.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class VerifyPaymentViewModel {\n");
    
    sb.append("  receiptData: ").append(receiptData).append("\n");
    sb.append("  paymentMethod: ").append(paymentMethod).append("\n");
    sb.append("  token: ").append(token).append("\n");
    sb.append("  planId: ").append(planId).append("\n");
    sb.append("  transactionId: ").append(transactionId).append("\n");
    sb.append("  productId: ").append(productId).append("\n");
    sb.append("  appType: ").append(appType).append("\n");
    sb.append("  intent: ").append(intent).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
