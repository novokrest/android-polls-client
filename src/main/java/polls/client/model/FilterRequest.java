/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * 
 **/
@ApiModel(description = "")
public class FilterRequest {
  
  @SerializedName("filterType")
  private Integer filterType = null;
  @SerializedName("optionId")
  private Long optionId = null;

  /**
   * 
   **/
  @ApiModelProperty(value = "")
  public Integer getFilterType() {
    return filterType;
  }
  public void setFilterType(Integer filterType) {
    this.filterType = filterType;
  }

  /**
   * 
   **/
  @ApiModelProperty(value = "")
  public Long getOptionId() {
    return optionId;
  }
  public void setOptionId(Long optionId) {
    this.optionId = optionId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FilterRequest filterRequest = (FilterRequest) o;
    return (this.filterType == null ? filterRequest.filterType == null : this.filterType.equals(filterRequest.filterType)) &&
        (this.optionId == null ? filterRequest.optionId == null : this.optionId.equals(filterRequest.optionId));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.filterType == null ? 0: this.filterType.hashCode());
    result = 31 * result + (this.optionId == null ? 0: this.optionId.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class FilterRequest {\n");
    
    sb.append("  filterType: ").append(filterType).append("\n");
    sb.append("  optionId: ").append(optionId).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
