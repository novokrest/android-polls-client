/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * 
 **/
@ApiModel(description = "")
public class LocationViewModel {
  
  @SerializedName("latitude")
  private Double latitude = null;
  @SerializedName("longitude")
  private Double longitude = null;
  @SerializedName("radius")
  private Long radius = null;

  /**
   * Gets or sets the latitude.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the latitude.")
  public Double getLatitude() {
    return latitude;
  }
  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  /**
   * Gets or sets the longitude.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the longitude.")
  public Double getLongitude() {
    return longitude;
  }
  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  /**
   * Gets or sets the radius.
   **/
  @ApiModelProperty(required = true, value = "Gets or sets the radius.")
  public Long getRadius() {
    return radius;
  }
  public void setRadius(Long radius) {
    this.radius = radius;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LocationViewModel locationViewModel = (LocationViewModel) o;
    return (this.latitude == null ? locationViewModel.latitude == null : this.latitude.equals(locationViewModel.latitude)) &&
        (this.longitude == null ? locationViewModel.longitude == null : this.longitude.equals(locationViewModel.longitude)) &&
        (this.radius == null ? locationViewModel.radius == null : this.radius.equals(locationViewModel.radius));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.latitude == null ? 0: this.latitude.hashCode());
    result = 31 * result + (this.longitude == null ? 0: this.longitude.hashCode());
    result = 31 * result + (this.radius == null ? 0: this.radius.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class LocationViewModel {\n");
    
    sb.append("  latitude: ").append(latitude).append("\n");
    sb.append("  longitude: ").append(longitude).append("\n");
    sb.append("  radius: ").append(radius).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
