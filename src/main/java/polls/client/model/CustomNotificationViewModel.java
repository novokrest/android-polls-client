/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import java.util.Date;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * Custom Notification ViewModel
 **/
@ApiModel(description = "Custom Notification ViewModel")
public class CustomNotificationViewModel {
  
  @SerializedName("noticationType")
  private String noticationType = null;
  @SerializedName("notificationTitle")
  private String notificationTitle = null;
  @SerializedName("notificationMessage")
  private String notificationMessage = null;
  @SerializedName("dateCreated")
  private Date dateCreated = null;
  @SerializedName("pollId")
  private Long pollId = null;
  @SerializedName("isDemographicCompleted")
  private Boolean isDemographicCompleted = null;
  @SerializedName("pollCompletedDate")
  private Date pollCompletedDate = null;
  @SerializedName("demographicCompletedDate")
  private Date demographicCompletedDate = null;
  @SerializedName("id")
  private Long id = null;
  @SerializedName("isActive")
  private Boolean isActive = null;

  /**
   * Gets or sets the type of the notication.
   **/
  @ApiModelProperty(value = "Gets or sets the type of the notication.")
  public String getNoticationType() {
    return noticationType;
  }
  public void setNoticationType(String noticationType) {
    this.noticationType = noticationType;
  }

  /**
   * Gets or sets the notification title.
   **/
  @ApiModelProperty(value = "Gets or sets the notification title.")
  public String getNotificationTitle() {
    return notificationTitle;
  }
  public void setNotificationTitle(String notificationTitle) {
    this.notificationTitle = notificationTitle;
  }

  /**
   * Gets or sets the notification message.
   **/
  @ApiModelProperty(value = "Gets or sets the notification message.")
  public String getNotificationMessage() {
    return notificationMessage;
  }
  public void setNotificationMessage(String notificationMessage) {
    this.notificationMessage = notificationMessage;
  }

  /**
   * Gets or sets the date created.
   **/
  @ApiModelProperty(value = "Gets or sets the date created.")
  public Date getDateCreated() {
    return dateCreated;
  }
  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }

  /**
   * Gets or sets the poll identifier.
   **/
  @ApiModelProperty(value = "Gets or sets the poll identifier.")
  public Long getPollId() {
    return pollId;
  }
  public void setPollId(Long pollId) {
    this.pollId = pollId;
  }

  /**
   * Gets or sets the is demographic completed.
   **/
  @ApiModelProperty(value = "Gets or sets the is demographic completed.")
  public Boolean getIsDemographicCompleted() {
    return isDemographicCompleted;
  }
  public void setIsDemographicCompleted(Boolean isDemographicCompleted) {
    this.isDemographicCompleted = isDemographicCompleted;
  }

  /**
   * Gets or sets the poll completed date.
   **/
  @ApiModelProperty(value = "Gets or sets the poll completed date.")
  public Date getPollCompletedDate() {
    return pollCompletedDate;
  }
  public void setPollCompletedDate(Date pollCompletedDate) {
    this.pollCompletedDate = pollCompletedDate;
  }

  /**
   * Gets or sets the demographic completed date.
   **/
  @ApiModelProperty(value = "Gets or sets the demographic completed date.")
  public Date getDemographicCompletedDate() {
    return demographicCompletedDate;
  }
  public void setDemographicCompletedDate(Date demographicCompletedDate) {
    this.demographicCompletedDate = demographicCompletedDate;
  }

  /**
   * Gets or sets the identifier.
   **/
  @ApiModelProperty(value = "Gets or sets the identifier.")
  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Gets or sets the is active.
   **/
  @ApiModelProperty(value = "Gets or sets the is active.")
  public Boolean getIsActive() {
    return isActive;
  }
  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CustomNotificationViewModel customNotificationViewModel = (CustomNotificationViewModel) o;
    return (this.noticationType == null ? customNotificationViewModel.noticationType == null : this.noticationType.equals(customNotificationViewModel.noticationType)) &&
        (this.notificationTitle == null ? customNotificationViewModel.notificationTitle == null : this.notificationTitle.equals(customNotificationViewModel.notificationTitle)) &&
        (this.notificationMessage == null ? customNotificationViewModel.notificationMessage == null : this.notificationMessage.equals(customNotificationViewModel.notificationMessage)) &&
        (this.dateCreated == null ? customNotificationViewModel.dateCreated == null : this.dateCreated.equals(customNotificationViewModel.dateCreated)) &&
        (this.pollId == null ? customNotificationViewModel.pollId == null : this.pollId.equals(customNotificationViewModel.pollId)) &&
        (this.isDemographicCompleted == null ? customNotificationViewModel.isDemographicCompleted == null : this.isDemographicCompleted.equals(customNotificationViewModel.isDemographicCompleted)) &&
        (this.pollCompletedDate == null ? customNotificationViewModel.pollCompletedDate == null : this.pollCompletedDate.equals(customNotificationViewModel.pollCompletedDate)) &&
        (this.demographicCompletedDate == null ? customNotificationViewModel.demographicCompletedDate == null : this.demographicCompletedDate.equals(customNotificationViewModel.demographicCompletedDate)) &&
        (this.id == null ? customNotificationViewModel.id == null : this.id.equals(customNotificationViewModel.id)) &&
        (this.isActive == null ? customNotificationViewModel.isActive == null : this.isActive.equals(customNotificationViewModel.isActive));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.noticationType == null ? 0: this.noticationType.hashCode());
    result = 31 * result + (this.notificationTitle == null ? 0: this.notificationTitle.hashCode());
    result = 31 * result + (this.notificationMessage == null ? 0: this.notificationMessage.hashCode());
    result = 31 * result + (this.dateCreated == null ? 0: this.dateCreated.hashCode());
    result = 31 * result + (this.pollId == null ? 0: this.pollId.hashCode());
    result = 31 * result + (this.isDemographicCompleted == null ? 0: this.isDemographicCompleted.hashCode());
    result = 31 * result + (this.pollCompletedDate == null ? 0: this.pollCompletedDate.hashCode());
    result = 31 * result + (this.demographicCompletedDate == null ? 0: this.demographicCompletedDate.hashCode());
    result = 31 * result + (this.id == null ? 0: this.id.hashCode());
    result = 31 * result + (this.isActive == null ? 0: this.isActive.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CustomNotificationViewModel {\n");
    
    sb.append("  noticationType: ").append(noticationType).append("\n");
    sb.append("  notificationTitle: ").append(notificationTitle).append("\n");
    sb.append("  notificationMessage: ").append(notificationMessage).append("\n");
    sb.append("  dateCreated: ").append(dateCreated).append("\n");
    sb.append("  pollId: ").append(pollId).append("\n");
    sb.append("  isDemographicCompleted: ").append(isDemographicCompleted).append("\n");
    sb.append("  pollCompletedDate: ").append(pollCompletedDate).append("\n");
    sb.append("  demographicCompletedDate: ").append(demographicCompletedDate).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("  isActive: ").append(isActive).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
