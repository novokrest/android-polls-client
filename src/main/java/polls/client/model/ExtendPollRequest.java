/**
 * Polls Web API
 * Poll Service Developers Web API
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package polls.client.model;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

/**
 * 
 **/
@ApiModel(description = "")
public class ExtendPollRequest {
  
  @SerializedName("pollId")
  private Long pollId = null;
  @SerializedName("expirationIncreamentInSeconds")
  private Long expirationIncreamentInSeconds = null;

  /**
   * 
   **/
  @ApiModelProperty(required = true, value = "")
  public Long getPollId() {
    return pollId;
  }
  public void setPollId(Long pollId) {
    this.pollId = pollId;
  }

  /**
   * 
   **/
  @ApiModelProperty(required = true, value = "")
  public Long getExpirationIncreamentInSeconds() {
    return expirationIncreamentInSeconds;
  }
  public void setExpirationIncreamentInSeconds(Long expirationIncreamentInSeconds) {
    this.expirationIncreamentInSeconds = expirationIncreamentInSeconds;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExtendPollRequest extendPollRequest = (ExtendPollRequest) o;
    return (this.pollId == null ? extendPollRequest.pollId == null : this.pollId.equals(extendPollRequest.pollId)) &&
        (this.expirationIncreamentInSeconds == null ? extendPollRequest.expirationIncreamentInSeconds == null : this.expirationIncreamentInSeconds.equals(extendPollRequest.expirationIncreamentInSeconds));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.pollId == null ? 0: this.pollId.hashCode());
    result = 31 * result + (this.expirationIncreamentInSeconds == null ? 0: this.expirationIncreamentInSeconds.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExtendPollRequest {\n");
    
    sb.append("  pollId: ").append(pollId).append("\n");
    sb.append("  expirationIncreamentInSeconds: ").append(expirationIncreamentInSeconds).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
