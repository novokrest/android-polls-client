package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestId;
import polls.client.model.BillingHistoryModel;
import polls.client.api.utils.UuidGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class BuyingHistoryTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        // when
        List<BillingHistoryModel> response = makeRequest(requestId);

        // then
        assertNotNull(response);
    }

    private List<BillingHistoryModel> makeRequest(final RequestId requestId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<List<BillingHistoryModel>>() {
            @Override
            public List<BillingHistoryModel> makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.buyingHistory(requestId); 
            }
        });
    }
}