package polls.client.api.pay;

import polls.client.api.PayApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class PayApiTestBase {

    private static final int TIMEOUT = 0;

    protected final PayApi api = new PayApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}