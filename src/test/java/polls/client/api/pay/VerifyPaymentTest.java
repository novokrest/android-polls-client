package polls.client.api.pay;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.VerifyPaymentViewModel;
import polls.client.model.PayResponse;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class VerifyPaymentTest extends PayApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        VerifyPaymentViewModel verifyPaymentViewModel = new VerifyPaymentViewModel();

        verifyPaymentViewModel.setReceiptData(WordGenerator.generateWord());
        verifyPaymentViewModel.setPaymentMethod(WordGenerator.generateWord());

        // when
        PayResponse response = makeRequest(verifyPaymentViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getCreditBalance());
        assertNotNull(response.getPollsBalance());
    }

    private PayResponse makeRequest(final VerifyPaymentViewModel verifyPaymentViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<PayResponse>() {
            @Override
            public PayResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.verifyPayment(verifyPaymentViewModel); 
            }
        });
    }
}