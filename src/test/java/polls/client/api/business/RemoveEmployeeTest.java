package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserRelation;
import polls.client.model.StatusResponse;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class RemoveEmployeeTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        UserRelation userRelation = new UserRelation();


        // when
        StatusResponse response = makeRequest(userRelation);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(final UserRelation userRelation) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<StatusResponse>() {
            @Override
            public StatusResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.removeEmployee(userRelation); 
            }
        });
    }
}