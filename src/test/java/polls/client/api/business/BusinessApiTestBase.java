package polls.client.api.business;

import polls.client.api.BusinessApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class BusinessApiTestBase {

    private static final int TIMEOUT = 0;

    protected final BusinessApi api = new BusinessApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}