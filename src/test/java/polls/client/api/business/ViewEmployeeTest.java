package polls.client.api.business;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.UserIdModel;
import polls.client.model.CompanyUsers;
import polls.client.api.utils.UuidGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ViewEmployeeTest extends BusinessApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        UserIdModel userIdModel = new UserIdModel();

        userIdModel.setId(UuidGenerator.generateUUID());

        // when
        CompanyUsers response = makeRequest(userIdModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getDepartmentId());
        assertNotNull(response.getBuyCompanyPolls());
        assertNotNull(response.getBuyTurkPolls());
        assertNotNull(response.getUserName());
        assertNotNull(response.getEmailAddress());
        assertNotNull(response.getPassword());
        assertNotNull(response.getUserType());
        assertNotNull(response.getId());
        assertNotNull(response.getNewDevice());
        assertNotNull(response.getNewProfileContact());
        assertNotNull(response.getOrg());
        assertNotNull(response.getVerificationMethod());
    }

    private CompanyUsers makeRequest(final UserIdModel userIdModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<CompanyUsers>() {
            @Override
            public CompanyUsers makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.viewEmployee(userIdModel); 
            }
        });
    }
}