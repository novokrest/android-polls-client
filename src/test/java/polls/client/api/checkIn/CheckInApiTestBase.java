package polls.client.api.checkIn;

import polls.client.api.CheckInApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class CheckInApiTestBase {

    private static final int TIMEOUT = 0;

    protected final CheckInApi api = new CheckInApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}