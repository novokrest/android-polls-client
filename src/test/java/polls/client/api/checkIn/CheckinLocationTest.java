package polls.client.api.checkIn;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestId;
import polls.client.model.LocationsList;
import polls.client.api.utils.UuidGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CheckinLocationTest extends CheckInApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        // when
        LocationsList response = makeRequest(requestId);

        // then
        assertNotNull(response);
        assertNotNull(response.getList());
    }

    private LocationsList makeRequest(final RequestId requestId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<LocationsList>() {
            @Override
            public LocationsList makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.checkinLocation(requestId); 
            }
        });
    }
}