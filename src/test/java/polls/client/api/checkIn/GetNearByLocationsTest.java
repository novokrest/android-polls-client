package polls.client.api.checkIn;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.LocationViewModel;
import polls.client.model.GoogleLocationViewModel;
import polls.client.api.utils.Random;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetNearByLocationsTest extends CheckInApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        LocationViewModel locationViewModel = new LocationViewModel();

        locationViewModel.setLatitude(Random.nextDouble(1.00D, 100.00D));
        locationViewModel.setLongitude(Random.nextDouble(1.00D, 100.00D));
        locationViewModel.setRadius(Random.nextLong(1L, 10L));

        // when
        GoogleLocationViewModel response = makeRequest(locationViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getResults());
        assertNotNull(response.getStatus());
    }

    private GoogleLocationViewModel makeRequest(final LocationViewModel locationViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<GoogleLocationViewModel>() {
            @Override
            public GoogleLocationViewModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getNearByLocations(locationViewModel); 
            }
        });
    }
}