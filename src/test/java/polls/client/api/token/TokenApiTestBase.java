package polls.client.api.token;

import polls.client.api.TokenApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class TokenApiTestBase {

    private static final int TIMEOUT = 0;

    protected final TokenApi api = new TokenApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}