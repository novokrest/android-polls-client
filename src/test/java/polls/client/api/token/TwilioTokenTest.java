package polls.client.api.token;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.TwilioVideoViewModel;
import polls.client.model.TokenContent;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class TwilioTokenTest extends TokenApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        TwilioVideoViewModel twilioVideoViewModel = new TwilioVideoViewModel();

        twilioVideoViewModel.setUserName(WordGenerator.generateName(10));
        twilioVideoViewModel.setRoomName(WordGenerator.generateName(10));

        // when
        TokenContent response = makeRequest(twilioVideoViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getExtExpiresIn());
        assertNotNull(response.getExpiresIn());
        assertNotNull(response.getAccessToken());
        assertNotNull(response.getExpiresOn());
        assertNotNull(response.getResource());
        assertNotNull(response.getNotBefore());
        assertNotNull(response.getTokenType());
    }

    private TokenContent makeRequest(final TwilioVideoViewModel twilioVideoViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<TokenContent>() {
            @Override
            public TokenContent makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.twilioToken(twilioVideoViewModel); 
            }
        });
    }
}