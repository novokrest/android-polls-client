package polls.client.api.auth;

import java.util.UUID;

public class Authorization {

    private final UUID userId;
    private final UUID token;

    public static Authorization of(UUID userId, UUID token) {
        return new Authorization(userId, token);
    }

    private Authorization(UUID userId, UUID token) {
        this.userId = userId;
        this.token = token;
    }

    public UUID getUserId() {
        return userId;
    }

    public UUID getToken() {
        return token;
    }
}
