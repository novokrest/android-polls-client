package polls.client.api.auth;

import polls.client.ApiException;
import polls.client.api.ApiInvokerEx;
import polls.client.api.PollsApi;
import polls.client.api.utils.DeviceModelGenerator;
import polls.client.model.ApiLoginModel;
import polls.client.model.LoginResponse;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class AuthRequestExecutor {

    private static final String VALID_USER_NAME = "samer";
    private static final String VALID_USER_PASSWORD = "123456";

    private final PollsApi api;

    public static AuthRequestExecutor create() {
        return new AuthRequestExecutor(new PollsApi());
    }

    private AuthRequestExecutor(PollsApi api) {
        this.api = api;
    }

    public interface ApiMethodInvoker<ResponseT> {
        ResponseT makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException;
    }

    public <ResponseT> ResponseT execute(ApiMethodInvoker<ResponseT> invoker) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        setAuthorization();
        return invoker.makeRequest();
    }

    private void setAuthorization() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        Authorization authorization = obtainAuthorization();
        ApiInvokerEx.setAuth(api.getInvoker(), authorization);
    }

    private Authorization obtainAuthorization() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        ApiLoginModel loginModel = new ApiLoginModel();
        loginModel.setUserName(VALID_USER_NAME);
        loginModel.setPassword(VALID_USER_PASSWORD);
        loginModel.setNewDevice(DeviceModelGenerator.generateDeviceModel());

        LoginResponse response = api.login(loginModel);

        return Authorization.of(response.getUserId(), response.getToken());
    }
}
