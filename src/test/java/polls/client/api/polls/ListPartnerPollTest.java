package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollRequest;
import polls.client.model.ListPollView;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ListPartnerPollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        PollRequest pollRequest = new PollRequest();


        // when
        ListPollView response = makeRequest(pollRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getItem());
    }

    private ListPollView makeRequest(final PollRequest pollRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<ListPollView>() {
            @Override
            public ListPollView makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.listPartnerPoll(pollRequest); 
            }
        });
    }
}