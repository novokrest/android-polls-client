package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FileObject;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.BooleanGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class UploadFileTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        String accessKey = WordGenerator.generateWord();

        Boolean isProfilePic = false;


        // when
        FileObject response = makeRequest(accessKey, isProfilePic);

        // then
        assertNotNull(response);
        assertNotNull(response.getId());
        assertNotNull(response.getFileID());
        assertNotNull(response.getContentType());
        assertNotNull(response.getUrl());
        assertNotNull(response.getFileName());
        assertNotNull(response.getFileDeleted());
        assertNotNull(response.getLastUpdate());
    }

    private FileObject makeRequest(final String accessKey, final Boolean isProfilePic) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<FileObject>() {
            @Override
            public FileObject makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.uploadFile(accessKey, isProfilePic); 
            }
        });
    }
}