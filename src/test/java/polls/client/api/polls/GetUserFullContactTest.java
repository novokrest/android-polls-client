package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FullContactModel;
import polls.client.model.FullContact;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetUserFullContactTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        FullContactModel fullContactModel = new FullContactModel();


        // when
        FullContact response = makeRequest(fullContactModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getRequestId());
        assertNotNull(response.getLikelihood());
        assertNotNull(response.getPhotos());
    }

    private FullContact makeRequest(final FullContactModel fullContactModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<FullContact>() {
            @Override
            public FullContact makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getUserFullContact(fullContactModel); 
            }
        });
    }
}