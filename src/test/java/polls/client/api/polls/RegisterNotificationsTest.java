package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.NotificationViewModel;
import polls.client.model.NotificationViewModel;
import polls.client.api.utils.UuidGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class RegisterNotificationsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        NotificationViewModel notificationViewModel = new NotificationViewModel();

        notificationViewModel.setDeviceId(UuidGenerator.generateUUID());

        // when
        NotificationViewModel response = makeRequest(notificationViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getDeviceId());
        assertNotNull(response.getEmail());
        assertNotNull(response.getBrowser());
        assertNotNull(response.getSms());
        assertNotNull(response.getPhoneCall());
        assertNotNull(response.getPushNotification());
    }

    private NotificationViewModel makeRequest(final NotificationViewModel notificationViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<NotificationViewModel>() {
            @Override
            public NotificationViewModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.registerNotifications(notificationViewModel); 
            }
        });
    }
}