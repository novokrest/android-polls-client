package polls.client.api.polls;

import polls.client.api.PollsApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class PollsApiTestBase {

    private static final int TIMEOUT = 0;

    protected final PollsApi api = new PollsApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}