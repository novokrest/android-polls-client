package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.DepartmentModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAllDepartmentTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given

        // when
        DepartmentModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getDepartmentId());
        assertNotNull(response.getDepartmentName());
        assertNotNull(response.getCreateDate());
        assertNotNull(response.getCreateBy());
        assertNotNull(response.getIsActive());
    }

    private DepartmentModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<DepartmentModel>() {
            @Override
            public DepartmentModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getAllDepartment(); 
            }
        });
    }
}