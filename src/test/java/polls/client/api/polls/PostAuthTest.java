package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ExternalProviderRequest;
import polls.client.model.LoginResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.BooleanGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class PostAuthTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        ExternalProviderRequest externalProviderRequest = new ExternalProviderRequest();

        externalProviderRequest.setProvider(0);
        externalProviderRequest.setId(WordGenerator.generateWord());
        externalProviderRequest.setVerifiedEmail(false);
        externalProviderRequest.setGivenName(WordGenerator.generateName(10));
        externalProviderRequest.setFamilyName(WordGenerator.generateName(10));
        externalProviderRequest.setAccessToken(WordGenerator.generateWord());

        // when
        LoginResponse response = makeRequest(externalProviderRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getUserName());
        assertNotNull(response.getDeviceId());
        assertNotNull(response.getIsPartner());
        assertNotNull(response.getAccessLevel());
        assertNotNull(response.getEmailAddress());
        assertNotNull(response.getUserId());
        assertNotNull(response.getToken());
        assertNotNull(response.getDisplayName());
        assertNotNull(response.getIsEmailVerified());
        assertNotNull(response.getIsPhoneVerified());
        assertNotNull(response.getIsCallVerified());
        assertNotNull(response.getUserType());
        assertNotNull(response.getSecretAccessKey());
    }

    private LoginResponse makeRequest(final ExternalProviderRequest externalProviderRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<LoginResponse>() {
            @Override
            public LoginResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.postAuth(externalProviderRequest); 
            }
        });
    }
}