package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RemoteImageViewModel;
import polls.client.model.FileObject;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class SaveRemoteImageTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RemoteImageViewModel remoteImageViewModel = new RemoteImageViewModel();

        remoteImageViewModel.setRemoteImageUrl(WordGenerator.generateWord());
        remoteImageViewModel.setAccessKey(WordGenerator.generateWord());

        // when
        FileObject response = makeRequest(remoteImageViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getId());
        assertNotNull(response.getFileID());
        assertNotNull(response.getContentType());
        assertNotNull(response.getUrl());
        assertNotNull(response.getFileName());
        assertNotNull(response.getFileDeleted());
        assertNotNull(response.getLastUpdate());
    }

    private FileObject makeRequest(final RemoteImageViewModel remoteImageViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<FileObject>() {
            @Override
            public FileObject makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.saveRemoteImage(remoteImageViewModel); 
            }
        });
    }
}