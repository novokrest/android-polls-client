package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ResetPassword;
import polls.client.model.PwdResetResponse;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ResetPasswordTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        ResetPassword resetPassword = new ResetPassword();

        resetPassword.setRequestId(WordGenerator.generateWord());
        resetPassword.setNewPassword(WordGenerator.generatePassword());
        resetPassword.setConfirmPassword(WordGenerator.generatePassword());

        // when
        PwdResetResponse response = makeRequest(resetPassword);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatusMessage());
    }

    private PwdResetResponse makeRequest(final ResetPassword resetPassword) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<PwdResetResponse>() {
            @Override
            public PwdResetResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.resetPassword(resetPassword); 
            }
        });
    }
}