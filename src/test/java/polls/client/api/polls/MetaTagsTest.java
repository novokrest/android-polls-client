package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.MetaTagViewModel;
import polls.client.model.MetaTagViewModel;
import polls.client.api.utils.Random;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class MetaTagsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        MetaTagViewModel metaTagViewModel = new MetaTagViewModel();

        metaTagViewModel.setPollId(Random.nextLong(1L, 10L));
        metaTagViewModel.setDescription(WordGenerator.generateWord());
        metaTagViewModel.setKeywords(WordGenerator.generateWord());

        // when
        MetaTagViewModel response = makeRequest(metaTagViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getPollId());
        assertNotNull(response.getTitle());
        assertNotNull(response.getAuthor());
        assertNotNull(response.getSubject());
        assertNotNull(response.getDescription());
        assertNotNull(response.getKeywords());
        assertNotNull(response.getClassification());
        assertNotNull(response.getGeography());
        assertNotNull(response.getLanguage());
        assertNotNull(response.getExpires());
        assertNotNull(response.getCopyright());
        assertNotNull(response.getDesigner());
        assertNotNull(response.getPublisher());
        assertNotNull(response.getDistribution());
        assertNotNull(response.getZipcode());
        assertNotNull(response.getCity());
        assertNotNull(response.getCountry());
    }

    private MetaTagViewModel makeRequest(final MetaTagViewModel metaTagViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<MetaTagViewModel>() {
            @Override
            public MetaTagViewModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.metaTags(metaTagViewModel); 
            }
        });
    }
}