package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ResetPasswordRequest;
import polls.client.model.PwdResetResponse;
import polls.client.api.utils.EmailGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ResetPasswordRequestTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();

        resetPasswordRequest.setEmailAddress(EmailGenerator.generateEmail());

        // when
        PwdResetResponse response = makeRequest(resetPasswordRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatusMessage());
    }

    private PwdResetResponse makeRequest(final ResetPasswordRequest resetPasswordRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<PwdResetResponse>() {
            @Override
            public PwdResetResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.resetPasswordRequest(resetPasswordRequest); 
            }
        });
    }
}