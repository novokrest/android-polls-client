package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterCategoryModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAllFilterCategoriesTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given

        // when
        FilterCategoryModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getCategoryName());
        assertNotNull(response.getFilter1());
        assertNotNull(response.getFilter2());
        assertNotNull(response.getFilter3());
        assertNotNull(response.getFilter4());
    }

    private FilterCategoryModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<FilterCategoryModel>() {
            @Override
            public FilterCategoryModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getAllFilterCategories(); 
            }
        });
    }
}