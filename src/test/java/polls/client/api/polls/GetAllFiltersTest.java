package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterRequest;
import polls.client.model.FilterData;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetAllFiltersTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        FilterRequest filterRequest = new FilterRequest();


        // when
        FilterData response = makeRequest(filterRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getFilters());
    }

    private FilterData makeRequest(final FilterRequest filterRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<FilterData>() {
            @Override
            public FilterData makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getAllFilters(filterRequest); 
            }
        });
    }
}