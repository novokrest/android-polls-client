package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.BestText;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;
import polls.client.api.utils.Random;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CreateBestTextPollTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        BestText bestText = new BestText();

        bestText.setFirstOption(WordGenerator.generateWord());
        bestText.setSecondOption(WordGenerator.generateWord());
        bestText.setPollTitle(WordGenerator.generateWord());
        bestText.setQuestion(WordGenerator.generateWord());
        bestText.setPollOverview(WordGenerator.generateWord());
        bestText.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        bestText.setKeywords(WordGenerator.generateWord());
        bestText.setTags(WordGenerator.generateWord());
        bestText.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        // when
        StatusResponse response = makeRequest(bestText);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(final BestText bestText) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<StatusResponse>() {
            @Override
            public StatusResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.createBestTextPoll(bestText); 
            }
        });
    }
}