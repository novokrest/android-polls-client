package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestNameModel;
import polls.client.model.StatesModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class StatesByCountryTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestNameModel requestNameModel = new RequestNameModel();


        // when
        List<StatesModel> response = makeRequest(requestNameModel);

        // then
        assertNotNull(response);
    }

    private List<StatesModel> makeRequest(final RequestNameModel requestNameModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<List<StatesModel>>() {
            @Override
            public List<StatesModel> makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.statesByCountry(requestNameModel); 
            }
        });
    }
}