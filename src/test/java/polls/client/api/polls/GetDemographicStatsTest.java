package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestPollId;
import polls.client.model.DemographicStats;
import polls.client.api.utils.Random;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetDemographicStatsTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestPollId requestPollId = new RequestPollId();

        requestPollId.setPollId(Random.nextLong(1L, 10L));

        // when
        DemographicStats response = makeRequest(requestPollId);

        // then
        assertNotNull(response);
        assertNotNull(response.getResponseCompleted());
        assertNotNull(response.getMaxAssignments());
        assertNotNull(response.getDemoCompleted());
        assertNotNull(response.getDemographicStatus());
    }

    private DemographicStats makeRequest(final RequestPollId requestPollId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<DemographicStats>() {
            @Override
            public DemographicStats makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getDemographicStats(requestPollId); 
            }
        });
    }
}