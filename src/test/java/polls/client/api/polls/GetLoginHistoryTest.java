package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.DeviceInfoModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetLoginHistoryTest extends PollsApiTestBase {

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given

        // when
        List<DeviceInfoModel> response = makeRequest();

        // then
        assertNotNull(response);
    }

    private List<DeviceInfoModel> makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return api.getLoginHistory(); 
    }
}