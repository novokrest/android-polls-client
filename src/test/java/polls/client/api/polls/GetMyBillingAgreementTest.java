package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.BillingAgreementViewModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetMyBillingAgreementTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given

        // when
        BillingAgreementViewModel response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getBillingId());
        assertNotNull(response.getUserId());
        assertNotNull(response.getPlanId());
        assertNotNull(response.getAgreementId());
        assertNotNull(response.getAgreementStatus());
        assertNotNull(response.getPayerEmail());
        assertNotNull(response.getFirstName());
        assertNotNull(response.getLastName());
        assertNotNull(response.getTimeStamp());
        assertNotNull(response.getDateCreated());
        assertNotNull(response.getTransactionType());
        assertNotNull(response.getCloseDate());
        assertNotNull(response.getIsActive());
        assertNotNull(response.getNextBillingDay());
        assertNotNull(response.getPayerId());
        assertNotNull(response.getPaymentGross());
        assertNotNull(response.getMaxAmount());
        assertNotNull(response.getPaymentStatus());
        assertNotNull(response.getMpStatus());
        assertNotNull(response.getReasonCode());
        assertNotNull(response.getMpDesc());
        assertNotNull(response.getPaymentMethod());
        assertNotNull(response.getCcBrand());
        assertNotNull(response.getCcLAst4());
        assertNotNull(response.getCcExpMonth());
        assertNotNull(response.getCCExpYear());
        assertNotNull(response.getCardId());
    }

    private BillingAgreementViewModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<BillingAgreementViewModel>() {
            @Override
            public BillingAgreementViewModel makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getMyBillingAgreement(); 
            }
        });
    }
}