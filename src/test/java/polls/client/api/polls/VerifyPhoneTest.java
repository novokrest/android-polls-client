package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.VerifyCodeViewModel;
import polls.client.model.StatusResponse;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class VerifyPhoneTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        VerifyCodeViewModel verifyCodeViewModel = new VerifyCodeViewModel();

        verifyCodeViewModel.setVerificationCode(WordGenerator.generateWord());
        verifyCodeViewModel.setUserName(WordGenerator.generateName(10));

        // when
        StatusResponse response = makeRequest(verifyCodeViewModel);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getStatusMessage());
    }

    private StatusResponse makeRequest(final VerifyCodeViewModel verifyCodeViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<StatusResponse>() {
            @Override
            public StatusResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.verifyPhone(verifyCodeViewModel); 
            }
        });
    }
}