package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestNameModel;
import polls.client.model.CitiesModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class CitiesByStateTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestNameModel requestNameModel = new RequestNameModel();


        // when
        List<CitiesModel> response = makeRequest(requestNameModel);

        // then
        assertNotNull(response);
    }

    private List<CitiesModel> makeRequest(final RequestNameModel requestNameModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<List<CitiesModel>>() {
            @Override
            public List<CitiesModel> makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.citiesByState(requestNameModel); 
            }
        });
    }
}