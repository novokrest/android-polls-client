package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.PollsStatsCounts;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPollCountersTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given

        // when
        PollsStatsCounts response = makeRequest();

        // then
        assertNotNull(response);
        assertNotNull(response.getCompleted());
        assertNotNull(response.getDraft());
        assertNotNull(response.getExpired());
        assertNotNull(response.getInProgress());
        assertNotNull(response.getPending());
        assertNotNull(response.getTotalPolls());
    }

    private PollsStatsCounts makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<PollsStatsCounts>() {
            @Override
            public PollsStatsCounts makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getPollCounters(); 
            }
        });
    }
}