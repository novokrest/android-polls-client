package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.FilterRequest;
import polls.client.model.FilterDataModel;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetFilterOptionsListTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        FilterRequest filterRequest = new FilterRequest();


        // when
        List<FilterDataModel> response = makeRequest(filterRequest);

        // then
        assertNotNull(response);
    }

    private List<FilterDataModel> makeRequest(final FilterRequest filterRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<List<FilterDataModel>>() {
            @Override
            public List<FilterDataModel> makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getFilterOptionsList(filterRequest); 
            }
        });
    }
}