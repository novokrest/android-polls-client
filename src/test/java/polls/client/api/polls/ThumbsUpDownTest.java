package polls.client.api.polls;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.ThumbsRequest;
import polls.client.model.ThumbsResponse;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ThumbsUpDownTest extends PollsApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        ThumbsRequest thumbsRequest = new ThumbsRequest();


        // when
        ThumbsResponse response = makeRequest(thumbsRequest);

        // then
        assertNotNull(response);
        assertNotNull(response.getStatus());
        assertNotNull(response.getRatingChoice());
    }

    private ThumbsResponse makeRequest(final ThumbsRequest thumbsRequest) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<ThumbsResponse>() {
            @Override
            public ThumbsResponse makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.thumbsUpDown(thumbsRequest); 
            }
        });
    }
}