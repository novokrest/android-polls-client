package polls.client.api.push;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SendMessageViewModel;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class SendWebPushNotificationsTestTest extends PushApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        SendMessageViewModel sendMessageViewModel = new SendMessageViewModel();

        sendMessageViewModel.setChannelId(WordGenerator.generateWord());
        sendMessageViewModel.setSnsType(WordGenerator.generateWord());
        sendMessageViewModel.setMessageToSend(WordGenerator.generateWord());

        // when
        String response = makeRequest(sendMessageViewModel);

        // then
        assertNotNull(response);
    }

    private String makeRequest(final SendMessageViewModel sendMessageViewModel) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<String>() {
            @Override
            public String makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.sendWebPushNotificationsTest(sendMessageViewModel); 
            }
        });
    }
}