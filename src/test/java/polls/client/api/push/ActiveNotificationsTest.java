package polls.client.api.push;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.RequestId;
import polls.client.model.ListCustomNotifications;
import polls.client.api.utils.UuidGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ActiveNotificationsTest extends PushApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        RequestId requestId = new RequestId();

        requestId.setId(UuidGenerator.generateUUID());

        // when
        ListCustomNotifications response = makeRequest(requestId);

        // then
        assertNotNull(response);
        assertNotNull(response.getItem());
    }

    private ListCustomNotifications makeRequest(final RequestId requestId) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<ListCustomNotifications>() {
            @Override
            public ListCustomNotifications makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.activeNotifications(requestId); 
            }
        });
    }
}