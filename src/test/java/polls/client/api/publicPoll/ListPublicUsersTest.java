package polls.client.api.publicPoll;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SearchOptions;
import polls.client.model.ContactList;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class ListPublicUsersTest extends PublicPollApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        SearchOptions searchOptions = new SearchOptions();


        // when
        ContactList response = makeRequest(searchOptions);

        // then
        assertNotNull(response);
        assertNotNull(response.getItem());
    }

    private ContactList makeRequest(final SearchOptions searchOptions) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<ContactList>() {
            @Override
            public ContactList makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.listPublicUsers(searchOptions); 
            }
        });
    }
}