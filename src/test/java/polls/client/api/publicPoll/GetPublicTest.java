package polls.client.api.publicPoll;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.SearchOptions;
import polls.client.model.ListPoll;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetPublicTest extends PublicPollApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        SearchOptions searchOptions = new SearchOptions();


        // when
        ListPoll response = makeRequest(searchOptions);

        // then
        assertNotNull(response);
        assertNotNull(response.getPollTypeId());
        assertNotNull(response.getFirstOption());
        assertNotNull(response.getSecondOption());
        assertNotNull(response.getFirstImagePath());
        assertNotNull(response.getSecondImagePath());
        assertNotNull(response.getChoiceFirstCounter());
        assertNotNull(response.getChoiceSecondCounter());
        assertNotNull(response.getResultCounter());
        assertNotNull(response.getFiltersJson());
        assertNotNull(response.getOwnerId());
        assertNotNull(response.getTotalCount());
        assertNotNull(response.getCompletedOn());
        assertNotNull(response.getDemographicCompleted());
        assertNotNull(response.getPollId());
        assertNotNull(response.getHitId());
        assertNotNull(response.getPollStatus());
        assertNotNull(response.getPollTitle());
        assertNotNull(response.getQuestion());
        assertNotNull(response.getPollOverview());
        assertNotNull(response.getAssignmentDurationInSeconds());
        assertNotNull(response.getKeywords());
        assertNotNull(response.getExpirationDate());
        assertNotNull(response.getFilterNameValue());
        assertNotNull(response.getFilterMainCategory());
        assertNotNull(response.getPUserName());
        assertNotNull(response.getIsEnablePublic());
        assertNotNull(response.getCatName());
        assertNotNull(response.getUserName());
        assertNotNull(response.getMaxAssignments());
        assertNotNull(response.getTags());
        assertNotNull(response.getLifeTimeInSeconds());
        assertNotNull(response.getIsEnabledComments());
        assertNotNull(response.getCreateDate());
        assertNotNull(response.getIsFilterOption());
        assertNotNull(response.getCatId());
        assertNotNull(response.getIsCompanyPoll());
        assertNotNull(response.getAlllowedAnonymously());
        assertNotNull(response.getIsAdult());
        assertNotNull(response.getIsPublished());
    }

    private ListPoll makeRequest(final SearchOptions searchOptions) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<ListPoll>() {
            @Override
            public ListPoll makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getPublic(searchOptions); 
            }
        });
    }
}