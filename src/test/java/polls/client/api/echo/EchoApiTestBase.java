package polls.client.api.echo;

import polls.client.api.EchoApi;
import polls.client.ApiInvoker;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class EchoApiTestBase {

    private static final int TIMEOUT = 0;

    protected final EchoApi api = new EchoApi();

    @Before
    public void before() {
        ApiInvoker.getInstance().setConnectionTimeout(TIMEOUT);
    }
}