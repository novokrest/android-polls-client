package polls.client.api.echo;

import polls.client.ApiException;
import polls.client.api.auth.AuthRequestExecutor;
import polls.client.model.HttpResponseMessage;
import polls.client.api.utils.WordGenerator;

import java.util.Date;
import java.util.UUID;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.Test;
import org.junit.Ignore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
public class GetEchoTest extends EchoApiTestBase {

    private final AuthRequestExecutor authRequestExecutor = AuthRequestExecutor.create();

    @Test
    public void testValidRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        // given
        String s = WordGenerator.generateWord();


        // when
        HttpResponseMessage response = makeRequest(s);

        // then
        assertNotNull(response);
        assertNotNull(response.getVersion());
        assertNotNull(response.getContent());
        assertNotNull(response.getStatusCode());
        assertNotNull(response.getReasonPhrase());
        assertNotNull(response.getHeaders());
        assertNotNull(response.getRequestMessage());
        assertNotNull(response.getIsSuccessStatusCode());
    }

    private HttpResponseMessage makeRequest(final String s) throws TimeoutException, ExecutionException, InterruptedException, ApiException {
        return authRequestExecutor.execute(new AuthRequestExecutor.ApiMethodInvoker<HttpResponseMessage>() {
            @Override
            public HttpResponseMessage makeRequest() throws TimeoutException, ExecutionException, InterruptedException, ApiException {
                    return api.getEcho(s); 
            }
        });
    }
}