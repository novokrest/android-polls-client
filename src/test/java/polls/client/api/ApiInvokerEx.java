package polls.client.api;

import polls.client.ApiInvoker;
import polls.client.api.auth.Authorization;
import polls.client.auth.ApiKeyAuth;


public final class ApiInvokerEx {

    private ApiInvokerEx() { }

    public static void setAuth(ApiInvoker api, Authorization authorization) {
        ApiKeyAuth userAuth = (ApiKeyAuth) api.getAuthentication("apiUserId");
        userAuth.setApiKey(authorization.getUserId().toString());

        ApiKeyAuth tokenAuth = (ApiKeyAuth) api.getAuthentication("apiToken");
        tokenAuth.setApiKey(authorization.getToken().toString());
    }
}



