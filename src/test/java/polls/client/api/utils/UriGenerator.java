package polls.client.api.utils;

import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UriGenerator {

    public static String generateUri() {
        return "http://test.com/test";
    }

    public static List<String> generateUriCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateUri()));
    }
}
