package polls.client.api.utils;

import polls.client.model.ListPoll;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListPollGenerator {

    public static ListPoll generateListPoll() {
        ListPoll listPoll = new ListPoll();

        listPoll.setPollTitle(WordGenerator.generateWord());
        listPoll.setQuestion(WordGenerator.generateWord());
        listPoll.setPollOverview(WordGenerator.generateWord());
        listPoll.setAssignmentDurationInSeconds(Random.nextLong(1L, 10L));
        listPoll.setKeywords(WordGenerator.generateWord());
        listPoll.setTags(WordGenerator.generateWord());
        listPoll.setLifeTimeInSeconds(Random.nextLong(1L, 10L));

        return listPoll;
    }

    public static List<ListPoll> generateListPollCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateListPoll()));
    }
}
