package polls.client.api.utils;

import polls.client.model.PaymentPlanViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PaymentPlanViewModelGenerator {

    public static PaymentPlanViewModel generatePaymentPlanViewModel() {
        PaymentPlanViewModel paymentPlanViewModel = new PaymentPlanViewModel();


        return paymentPlanViewModel;
    }

    public static List<PaymentPlanViewModel> generatePaymentPlanViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePaymentPlanViewModel()));
    }
}
