package polls.client.api.utils;

import polls.client.model.Northeast;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NortheastGenerator {

    public static Northeast generateNortheast() {
        Northeast northeast = new Northeast();


        return northeast;
    }

    public static List<Northeast> generateNortheastCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateNortheast()));
    }
}
