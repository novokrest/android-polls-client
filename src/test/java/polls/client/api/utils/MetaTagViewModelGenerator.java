package polls.client.api.utils;

import polls.client.model.MetaTagViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MetaTagViewModelGenerator {

    public static MetaTagViewModel generateMetaTagViewModel() {
        MetaTagViewModel metaTagViewModel = new MetaTagViewModel();

        metaTagViewModel.setPollId(Random.nextLong(1L, 10L));
        metaTagViewModel.setDescription(WordGenerator.generateWord());
        metaTagViewModel.setKeywords(WordGenerator.generateWord());

        return metaTagViewModel;
    }

    public static List<MetaTagViewModel> generateMetaTagViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateMetaTagViewModel()));
    }
}
