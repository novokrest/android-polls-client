package polls.client.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PhoneGenerator {

    private PhoneGenerator() { }

    public static String generatePhone() {
        return WordGenerator.generatePhoneNumber();
    }
}
