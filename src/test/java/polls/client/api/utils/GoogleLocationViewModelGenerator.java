package polls.client.api.utils;

import polls.client.model.GoogleLocationViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GoogleLocationViewModelGenerator {

    public static GoogleLocationViewModel generateGoogleLocationViewModel() {
        GoogleLocationViewModel googleLocationViewModel = new GoogleLocationViewModel();


        return googleLocationViewModel;
    }

    public static List<GoogleLocationViewModel> generateGoogleLocationViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateGoogleLocationViewModel()));
    }
}
