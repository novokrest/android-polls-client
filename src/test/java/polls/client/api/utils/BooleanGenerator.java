package polls.client.api.utils;

import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BooleanGenerator {

    public static boolean generateBoolean() {
        return false;
    }

    public static List<Boolean> generateBooleans() {
        return Collections.unmodifiableList(Arrays.asList(generateBoolean()));
    }
}
