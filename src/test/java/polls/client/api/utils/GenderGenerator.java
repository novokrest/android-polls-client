package polls.client.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class GenderGenerator {

    private static List<String> GENDERS = Collections.unmodifiableList(Arrays.asList(
            "male", "female"
    ));

    private GenderGenerator() { }

    public static String generateGender() {
        int index = Random.nextInt(0, GENDERS.size());
        return GENDERS.get(index);
    }
}
