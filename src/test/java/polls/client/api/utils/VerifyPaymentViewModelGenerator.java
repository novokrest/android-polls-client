package polls.client.api.utils;

import polls.client.model.VerifyPaymentViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class VerifyPaymentViewModelGenerator {

    public static VerifyPaymentViewModel generateVerifyPaymentViewModel() {
        VerifyPaymentViewModel verifyPaymentViewModel = new VerifyPaymentViewModel();

        verifyPaymentViewModel.setReceiptData(WordGenerator.generateWord());
        verifyPaymentViewModel.setPaymentMethod(WordGenerator.generateWord());

        return verifyPaymentViewModel;
    }

    public static List<VerifyPaymentViewModel> generateVerifyPaymentViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateVerifyPaymentViewModel()));
    }
}
