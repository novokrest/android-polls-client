package polls.client.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Random {

    private static java.util.Random RANDOM = new java.util.Random();

    public static long nextLong(long from, long to) {
        return nextInt((int) from, (int) to);
    }

    public static List<Long> nextLongs(long from, long to) {
        return Collections.unmodifiableList(Arrays.asList(nextLong(from, to)));
    }

    public static int nextInt(int from, int to) {
        return RANDOM.nextInt(to - from) + from;
    }

    public static List<Integer> nextInts(int from, int to) {
        return Collections.unmodifiableList(Arrays.asList(nextInt(from, to)));
    }

    public static double nextDouble(double from, double to) {
        return RANDOM.nextDouble() * (to - from) + from;
    }

    public static List<Double> nextDoubles(double from, double to) {
        return Collections.unmodifiableList(Arrays.asList(nextDouble(from, to)));
    }
}
