package polls.client.api.utils;

import polls.client.model.CustomNotificationViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CustomNotificationViewModelGenerator {

    public static CustomNotificationViewModel generateCustomNotificationViewModel() {
        CustomNotificationViewModel customNotificationViewModel = new CustomNotificationViewModel();


        return customNotificationViewModel;
    }

    public static List<CustomNotificationViewModel> generateCustomNotificationViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCustomNotificationViewModel()));
    }
}
