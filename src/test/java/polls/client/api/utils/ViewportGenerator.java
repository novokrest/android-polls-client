package polls.client.api.utils;

import polls.client.model.Viewport;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ViewportGenerator {

    public static Viewport generateViewport() {
        Viewport viewport = new Viewport();


        return viewport;
    }

    public static List<Viewport> generateViewportCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateViewport()));
    }
}
