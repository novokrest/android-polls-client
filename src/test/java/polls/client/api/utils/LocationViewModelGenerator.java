package polls.client.api.utils;

import polls.client.model.LocationViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LocationViewModelGenerator {

    public static LocationViewModel generateLocationViewModel() {
        LocationViewModel locationViewModel = new LocationViewModel();

        locationViewModel.setLatitude(Random.nextDouble(1.00D, 100.00D));
        locationViewModel.setLongitude(Random.nextDouble(1.00D, 100.00D));
        locationViewModel.setRadius(Random.nextLong(1L, 10L));

        return locationViewModel;
    }

    public static List<LocationViewModel> generateLocationViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateLocationViewModel()));
    }
}
