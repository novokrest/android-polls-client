package polls.client.api.utils;

import polls.client.model.ListBillingHistory;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListBillingHistoryGenerator {

    public static ListBillingHistory generateListBillingHistory() {
        ListBillingHistory listBillingHistory = new ListBillingHistory();


        return listBillingHistory;
    }

    public static List<ListBillingHistory> generateListBillingHistoryCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateListBillingHistory()));
    }
}
