package polls.client.api.utils;

import polls.client.model.TwilioVideoViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TwilioVideoViewModelGenerator {

    public static TwilioVideoViewModel generateTwilioVideoViewModel() {
        TwilioVideoViewModel twilioVideoViewModel = new TwilioVideoViewModel();

        twilioVideoViewModel.setUserName(WordGenerator.generateName(10));
        twilioVideoViewModel.setRoomName(WordGenerator.generateName(10));

        return twilioVideoViewModel;
    }

    public static List<TwilioVideoViewModel> generateTwilioVideoViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateTwilioVideoViewModel()));
    }
}
