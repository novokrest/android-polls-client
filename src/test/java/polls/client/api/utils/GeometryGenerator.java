package polls.client.api.utils;

import polls.client.model.Geometry;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GeometryGenerator {

    public static Geometry generateGeometry() {
        Geometry geometry = new Geometry();


        return geometry;
    }

    public static List<Geometry> generateGeometryCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateGeometry()));
    }
}
