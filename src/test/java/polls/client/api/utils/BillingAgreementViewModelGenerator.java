package polls.client.api.utils;

import polls.client.model.BillingAgreementViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BillingAgreementViewModelGenerator {

    public static BillingAgreementViewModel generateBillingAgreementViewModel() {
        BillingAgreementViewModel billingAgreementViewModel = new BillingAgreementViewModel();


        return billingAgreementViewModel;
    }

    public static List<BillingAgreementViewModel> generateBillingAgreementViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateBillingAgreementViewModel()));
    }
}
