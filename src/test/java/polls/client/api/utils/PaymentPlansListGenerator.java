package polls.client.api.utils;

import polls.client.model.PaymentPlansList;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PaymentPlansListGenerator {

    public static PaymentPlansList generatePaymentPlansList() {
        PaymentPlansList paymentPlansList = new PaymentPlansList();


        return paymentPlansList;
    }

    public static List<PaymentPlansList> generatePaymentPlansListCollection() {
        return Collections.unmodifiableList(Arrays.asList(generatePaymentPlansList()));
    }
}
