package polls.client.api.utils;

import polls.client.model.Southwest;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SouthwestGenerator {

    public static Southwest generateSouthwest() {
        Southwest southwest = new Southwest();


        return southwest;
    }

    public static List<Southwest> generateSouthwestCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateSouthwest()));
    }
}
