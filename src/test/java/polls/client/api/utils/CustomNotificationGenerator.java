package polls.client.api.utils;

import polls.client.model.CustomNotification;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CustomNotificationGenerator {

    public static CustomNotification generateCustomNotification() {
        CustomNotification customNotification = new CustomNotification();


        return customNotification;
    }

    public static List<CustomNotification> generateCustomNotificationCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCustomNotification()));
    }
}
