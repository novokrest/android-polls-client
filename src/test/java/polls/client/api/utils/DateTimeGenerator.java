package polls.client.api.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DateTimeGenerator {

    public static Date currentDateTime() {
        return new Date();
    }

    public static List<Date> generateDateTimes() {
        return Collections.unmodifiableList(Arrays.asList(currentDateTime()));
    }
}