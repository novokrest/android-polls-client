package polls.client.api.utils;

import polls.client.model.GCMViewModel;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GCMViewModelGenerator {

    public static GCMViewModel generateGCMViewModel() {
        GCMViewModel gCMViewModel = new GCMViewModel();

        gCMViewModel.setSnsDeviceId(WordGenerator.generateWord());
        gCMViewModel.setMessageToSend(WordGenerator.generateWord());
        gCMViewModel.setSnsType(WordGenerator.generateWord());

        return gCMViewModel;
    }

    public static List<GCMViewModel> generateGCMViewModelCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateGCMViewModel()));
    }
}
