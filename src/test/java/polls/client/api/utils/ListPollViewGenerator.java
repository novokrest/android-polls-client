package polls.client.api.utils;

import polls.client.model.ListPollView;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListPollViewGenerator {

    public static ListPollView generateListPollView() {
        ListPollView listPollView = new ListPollView();


        return listPollView;
    }

    public static List<ListPollView> generateListPollViewCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateListPollView()));
    }
}
