package polls.client.api.utils;

import polls.client.model.ListCustomNotifications;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListCustomNotificationsGenerator {

    public static ListCustomNotifications generateListCustomNotifications() {
        ListCustomNotifications listCustomNotifications = new ListCustomNotifications();


        return listCustomNotifications;
    }

    public static List<ListCustomNotifications> generateListCustomNotificationsCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateListCustomNotifications()));
    }
}
