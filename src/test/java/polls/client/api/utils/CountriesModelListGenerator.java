package polls.client.api.utils;

import polls.client.model.CountriesModelList;
import java.util.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountriesModelListGenerator {

    public static CountriesModelList generateCountriesModelList() {
        CountriesModelList countriesModelList = new CountriesModelList();


        return countriesModelList;
    }

    public static List<CountriesModelList> generateCountriesModelListCollection() {
        return Collections.unmodifiableList(Arrays.asList(generateCountriesModelList()));
    }
}
