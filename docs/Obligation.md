
# Obligation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**obligationCode** | **String** | Gets or sets the obligation code. |  [optional]
**obligationDescription** | **String** | Gets or sets the obligation description. |  [optional]



