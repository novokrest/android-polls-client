
# UserCreditModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) |  |  [optional]
**dateCreated** | [**Date**](Date.md) |  |  [optional]
**balanceAmount** | **Double** |  |  [optional]
**modifiedDate** | [**Date**](Date.md) |  |  [optional]
**creditBalance** | **Long** |  |  [optional]
**pollsBalance** | **Long** |  |  [optional]
**nextBillingDay** | **Long** |  |  [optional]
**lastPaymentDate** | [**Date**](Date.md) |  |  [optional]
**memberSince** | [**Date**](Date.md) |  |  [optional]



