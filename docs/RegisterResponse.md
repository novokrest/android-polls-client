
# RegisterResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) |  | 
**token** | [**UUID**](UUID.md) |  | 
**displayName** | **String** |  |  [optional]
**accessLevel** | **Long** |  |  [optional]
**message** | **String** |  |  [optional]
**isEmailVerified** | **Boolean** |  |  [optional]
**isPhoneVerified** | **Boolean** |  |  [optional]
**isCallVerified** | **Boolean** |  |  [optional]



