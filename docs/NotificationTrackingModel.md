
# NotificationTrackingModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**pollId** | **Long** |  |  [optional]
**messageTitle** | **String** |  |  [optional]
**messageText** | **String** |  |  [optional]
**alertType** | **String** |  |  [optional]
**dateCreated** | [**Date**](Date.md) |  |  [optional]
**channelId** | **String** |  |  [optional]



