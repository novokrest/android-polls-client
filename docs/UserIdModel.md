
# UserIdModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**UUID**](UUID.md) | The identifier | 
**viewAll** | **Boolean** | The view all |  [optional]



