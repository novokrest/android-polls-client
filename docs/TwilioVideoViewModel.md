
# TwilioVideoViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** | Gets or sets the name of the user. | 
**roomName** | **String** | Gets or sets the name of the room. | 



