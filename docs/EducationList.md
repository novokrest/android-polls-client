
# EducationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**List&lt;EducationModel&gt;**](EducationModel.md) |  |  [optional]



