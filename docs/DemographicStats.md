
# DemographicStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**responseCompleted** | **Long** |  |  [optional]
**maxAssignments** | **Long** |  |  [optional]
**demoCompleted** | **Long** |  |  [optional]
**demographicStatus** | **String** |  |  [optional]



