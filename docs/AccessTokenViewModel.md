
# AccessTokenViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope** | **String** | Gets or sets the scope. |  [optional]
**nonce** | **String** | Gets or sets the nonce. |  [optional]
**obligations** | [**List&lt;Obligation&gt;**](Obligation.md) | Gets or sets the obligations. |  [optional]
**accessToken** | **String** | Gets or sets the access token. |  [optional]
**tokenType** | **String** | Gets or sets the type of the token. |  [optional]
**expiresIn** | **Long** | Gets or sets the expires in. |  [optional]
**refreshToken** | **String** | Gets or sets the refresh token. |  [optional]



