# EchoApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEcho**](EchoApi.md#getEcho) | **GET** /v1/Echo/GetEcho | Gets the echo.


<a name="getEcho"></a>
# **getEcho**
> HttpResponseMessage getEcho(s)

Gets the echo.

Gets the echo.

### Example
```java
// Import classes:
//import polls.client.api.EchoApi;

EchoApi apiInstance = new EchoApi();
String s = "s_example"; // String | The s.
try {
    HttpResponseMessage result = apiInstance.getEcho(s);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EchoApi#getEcho");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **s** | **String**| The s. |

### Return type

[**HttpResponseMessage**](HttpResponseMessage.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

