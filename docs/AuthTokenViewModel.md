
# AuthTokenViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**platform** | **String** | Gets or sets the platform. | 
**environment** | **String** | Gets or sets the environment. | 
**code** | **String** | Gets or sets the code. | 
**productName** | **String** | Gets or sets the name of the product. |  [optional]
**responseType** | **String** | Gets or sets the type of the response. |  [optional]
**paymentMethod** | **Integer** | The payment method |  [optional]



