
# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **Double** | Gets or sets the latitude. |  [optional]
**lng** | **Double** | Gets or sets the longitude. |  [optional]
**radius** | **Long** | Gets or sets the radius. |  [optional]



