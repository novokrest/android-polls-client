
# NotificationViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceId** | [**UUID**](UUID.md) | device identifier. | 
**email** | **Boolean** | Want to recieve email notifications. |  [optional]
**browser** | **Boolean** | Want to recieve  browser notifications. |  [optional]
**sms** | **Boolean** | Want to recieve  SMS. |  [optional]
**phoneCall** | **Boolean** | Want to recieve phone call. |  [optional]
**pushNotification** | **Boolean** | Want to recieve push notification. |  [optional]



