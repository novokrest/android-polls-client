
# ResetPassword

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **String** | Gets or sets the request identifier. | 
**newPassword** | **String** | Gets or sets the new password. | 
**confirmPassword** | **String** | Gets or sets the confirm password. | 



