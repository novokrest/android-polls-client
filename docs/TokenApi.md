# TokenApi

All URIs are relative to *http://demo.honeyshyam.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rooms**](TokenApi.md#rooms) | **POST** /v1/Token/Rooms | 
[**twilioToken**](TokenApi.md#twilioToken) | **POST** /v1/Token/TwilioToken | Get TwilioToken to make a Audio/Video connection


<a name="rooms"></a>
# **rooms**
> HttpResponseMessage rooms()





### Example
```java
// Import classes:
//import polls.client.api.TokenApi;

TokenApi apiInstance = new TokenApi();
try {
    HttpResponseMessage result = apiInstance.rooms();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#rooms");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**HttpResponseMessage**](HttpResponseMessage.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="twilioToken"></a>
# **twilioToken**
> TokenContent twilioToken(twilioVideoViewModel)

Get TwilioToken to make a Audio/Video connection

Get TwilioToken to make a Audio/Video connection

### Example
```java
// Import classes:
//import polls.client.api.TokenApi;

TokenApi apiInstance = new TokenApi();
TwilioVideoViewModel twilioVideoViewModel = new TwilioVideoViewModel(); // TwilioVideoViewModel | 
try {
    TokenContent result = apiInstance.twilioToken(twilioVideoViewModel);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TokenApi#twilioToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **twilioVideoViewModel** | [**TwilioVideoViewModel**](TwilioVideoViewModel.md)|  |

### Return type

[**TokenContent**](TokenContent.md)

### Authorization

[apiToken](../README.md#apiToken), [apiUserId](../README.md#apiUserId)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

