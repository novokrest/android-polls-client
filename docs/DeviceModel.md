
# DeviceModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deviceName** | **String** | The device name | 
**devicePlatform** | **String** | The device platform             Depending on the device, a few examples are:             - \&quot;Android\&quot;             - \&quot;BlackBerry 10\&quot;             - \&quot;browser\&quot;             - \&quot;iOS\&quot;             - \&quot;WinCE\&quot;             - \&quot;Tizen\&quot;             - \&quot;Mac OS X\&quot;             Windows | 
**snsDeviceId** | **String** | The SNS device identifier | 
**channelId** | **String** | Gets or sets the channel identifier. |  [optional]
**deviceVersion** | **String** | The device version |  [optional]
**browserName** | **String** | The browser name |  [optional]
**browserVersion** | **String** | The browser version |  [optional]
**browserUserAgent** | **String** | The browser user agent |  [optional]
**snsType** | **String** | The SNS type  \&quot;APNS\&quot; - Apple Push Notification Service,   \&quot;GCM\&quot; - Google Cloud Messaging, WEB |  [optional]



