
# Result

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geometry** | [**Geometry**](Geometry.md) | Gets or sets the geometry. |  [optional]
**icon** | **String** | Gets or sets the icon. |  [optional]
**id** | **String** | Gets or sets the identifier. |  [optional]
**name** | **String** | Gets or sets the name. |  [optional]
**photos** | [**List&lt;Photo&gt;**](Photo.md) | Gets or sets the photos. |  [optional]
**placeId** | **String** | Gets or sets the place identifier. |  [optional]
**reference** | **String** | Gets or sets the reference. |  [optional]
**scope** | **String** | Gets or sets the scope. |  [optional]
**types** | **List&lt;String&gt;** | Gets or sets the types. |  [optional]
**vicinity** | **String** | Gets or sets the vicinity. |  [optional]
**rating** | **Double** | Gets or sets the rating. |  [optional]



