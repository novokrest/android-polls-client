
# SendMessageViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** | Gets or sets the channel identifier. | 
**snsType** | **String** | Gets or sets the type of the SNS WEB, APN, GCM. | 
**messageToSend** | **String** | Gets or sets the message to send. | 
**userId** | [**UUID**](UUID.md) | Gets or sets the user identifier. |  [optional]
**pollId** | **Long** | Gets or sets the poll identifier. |  [optional]



