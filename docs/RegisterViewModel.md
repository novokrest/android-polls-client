
# RegisterViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** | the name of the user. | 
**emailAddress** | **String** | the email address. | 
**password** | **String** | the password. | 
**userType** | **Integer** | the type of the user. |  [optional]
**id** | [**UUID**](UUID.md) | the identifier. |  [optional]
**newDevice** | [**DeviceModel**](DeviceModel.md) | the new device info need to be send. | 
**newProfileContact** | [**Contact**](Contact.md) | the new profile contact. | 
**org** | **String** | The org Business account flag &#x3D; Blank, \&quot;owner\&quot; or \&quot;member\&quot;, set to \&quot;create\&quot;, \&quot;delete\&quot; to remove |  [optional]
**verificationMethod** | **Integer** | The verificati method values can be EMAIL,SMS,PHONE,NONE |  [optional]



