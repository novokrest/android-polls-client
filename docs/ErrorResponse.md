
# ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | error code |  [optional]
**message** | **String** | error message |  [optional]



