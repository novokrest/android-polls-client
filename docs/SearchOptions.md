
# SearchOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userName** | **String** | the name of the user. |  [optional]
**createDate** | **String** | the create date time. |  [optional]
**catId** | **Long** | the create date time. |  [optional]
**userId** | [**UUID**](UUID.md) | the userId. |  [optional]
**categoryName** | **String** |  |  [optional]
**pollStatus** | **Integer** |  |  [optional]
**pageSize** | **Long** |  |  [optional]
**pageNumber** | **Long** | The page number |  [optional]
**searchParam** | **Integer** | The search Parameter,  UserName &#x3D; 1, |  [optional]
**sortOrder** | **Integer** |  |  [optional]



