
# FilterValues

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filterValue** | **String** |  |  [optional]
**filterCount** | **Long** |  |  [optional]
**optionA** | **Long** |  |  [optional]
**optionB** | **Long** |  |  [optional]



