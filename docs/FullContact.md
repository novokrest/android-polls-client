
# FullContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Long** |  |  [optional]
**requestId** | **String** |  |  [optional]
**likelihood** | **Double** |  |  [optional]
**photos** | **String** |  |  [optional]



