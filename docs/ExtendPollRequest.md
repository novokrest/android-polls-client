
# ExtendPollRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pollId** | **Long** |  | 
**expirationIncreamentInSeconds** | **Long** |  | 



