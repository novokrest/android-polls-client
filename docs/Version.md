
# Version

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**major** | **Long** |  |  [optional]
**minor** | **Long** |  |  [optional]
**build** | **Long** |  |  [optional]
**revision** | **Long** |  |  [optional]
**majorRevision** | **Long** |  |  [optional]
**minorRevision** | **Long** |  |  [optional]



