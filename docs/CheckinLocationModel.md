
# CheckinLocationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**latitude** | **Double** |  |  [optional]
**longitude** | **Double** |  |  [optional]
**dateCreated** | [**Date**](Date.md) |  |  [optional]
**ipAddress** | **String** |  |  [optional]
**timeStamp** | [**Date**](Date.md) |  |  [optional]
**userId** | [**UUID**](UUID.md) |  |  [optional]
**locationName** | **String** |  |  [optional]



