
# VerifyCodeViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verificationCode** | **String** | the verification code. | 
**userName** | **String** | the Username. | 
**verificationMethod** | **String** |  |  [optional]



