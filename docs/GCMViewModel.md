
# GCMViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**snsDeviceId** | **String** | Gets or sets the SNS device identifier. | 
**messageToSend** | **String** | Gets or sets the message to send. | 
**snsType** | **String** | Gets or sets the type of the SNS. | 



