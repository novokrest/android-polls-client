
# ListCustomNotifications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | [**List&lt;CustomNotificationViewModel&gt;**](CustomNotificationViewModel.md) |  |  [optional]



