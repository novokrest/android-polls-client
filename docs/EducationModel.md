
# EducationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**educationId** | **Long** |  |  [optional]
**educationName** | **String** |  |  [optional]



