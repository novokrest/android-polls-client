
# CountriesModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**countryName** | **String** |  |  [optional]
**phoneCode** | **String** |  |  [optional]



