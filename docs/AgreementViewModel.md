
# AgreementViewModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planId** | [**UUID**](UUID.md) |  | 
**agreementId** | **String** |  | 
**agreementStatus** | **String** |  |  [optional]
**payerEmail** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**timeStamp** | **String** |  |  [optional]
**isActive** | **Boolean** |  |  [optional]
**nextBillingDay** | **Long** |  | 
**payerId** | **String** |  | 
**paymentGross** | **Double** |  | 
**maxAmount** | **Double** |  |  [optional]
**paymentStatus** | **String** |  |  [optional]
**mpStatus** | **Long** |  |  [optional]
**reasonCode** | **String** |  |  [optional]
**mpDesc** | **String** |  |  [optional]
**paymentMethod** | **String** |  | 
**cCBrand** | **String** |  |  [optional]
**cCLAst4** | **Long** |  |  [optional]
**cCExpMonth** | **Long** |  |  [optional]
**cCExpYear** | **Long** |  |  [optional]
**cardId** | **String** |  |  [optional]



