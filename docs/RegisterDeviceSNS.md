
# RegisterDeviceSNS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | [**UUID**](UUID.md) |  |  [optional]
**snsType** | **String** |  |  [optional]
**snsAppId** | **String** |  |  [optional]
**snsDeviceId** | **String** |  |  [optional]
**snsDeviceId2** | **String** |  |  [optional]
**channelId** | **String** |  |  [optional]
**deviceId** | [**UUID**](UUID.md) |  |  [optional]



